
export default {
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',
  buildDir: 'nuxt-dist',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' },
      { name: 'theme-color', content: '$EA0029' },
    ],
    link: [
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css' },
      { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.8.2/css/all.css', integrity: 'sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay', crossorigin: 'anonymous' },
      { rel: 'icon', type: 'image/x-icon', href: '/iso.svg' }
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/sass/styles.scss',
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    '~/plugins/i18n',
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxt/typescript-build',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    'nuxt-i18n',
    '@nuxtjs/style-resources',
  ],
  i18n: {
    lazy: true,
    strategy: 'prefix',
    locales: [
      { code: 'es', iso: 'es-ES', file: 'es.js', name: 'Español' },
      { code: 'en', iso: 'en-US', file: 'en.js', name: 'English' },
    ],
    langDir: 'locales/',
    defaultLocale: 'en',
  },
  // Common imports
  styleResources: {
    scss: [
      '~/assets/sass/colors.scss',
    ]
  },
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  /* ** Build configuration */
  build: {
    babel: {
      presets({ isServer }) {
        const targets = isServer ? { node: 'current' } : { ie: 11 }
        return [
          [require.resolve('@nuxt/babel-preset-app'), { targets }]
        ]
      }
    }    
  },
  publicRuntimeConfig: {
    emailjsService: "service_4dnc17m",
    emailjsUserId: "user_efRZFxg3PyIJ2BLrvSFqL",
    emailjsContact: "template_qwrpu3c",
    emailjsCV: "info@gobetweenlsp.com",
  }
}
