export const CatalogServices = [
  {
    name: 'catalogs.services.field-locale.title',
    description: 'catalogs.services.field-locale.description',
    subtitle: 'catalogs.services.field-locale.subtitle',
    icon: 'fas fa-crosshairs',
    rotateIcon: false,
    services: [
      'catalogs.services.field-locale.service-technology',
      'catalogs.services.field-locale.service-telecommunications',
      'catalogs.services.field-locale.service-software',
      'catalogs.services.field-locale.service-life-sciences',
      'catalogs.services.field-locale.service-gaming',
      'catalogs.services.field-locale.service-legal',
      'catalogs.services.field-locale.service-education',
      'catalogs.services.field-locale.service-travel',
      'catalogs.services.field-locale.service-automotive',
      'catalogs.services.field-locale.service-more',
    ]
  },
  {
    name: 'catalogs.services.cat-tools.title',
    description: 'catalogs.services.cat-tools.description',
    icon: 'fas fa-wrench',
    rotateIcon: true,
    services: [
      'catalogs.services.cat-tools.service-linguistic',
      'catalogs.services.cat-tools.service-catalyst',
      'catalogs.services.cat-tools.service-across',
      'catalogs.services.cat-tools.service-trados-studio',
      'catalogs.services.cat-tools.service-translation',
      'catalogs.services.cat-tools.service-smartling',
      'catalogs.services.cat-tools.service-passolo',
      'catalogs.services.cat-tools.service-wordfast',
      'catalogs.services.cat-tools.service-memoQ',
      'catalogs.services.cat-tools.service-xtm',
      'catalogs.services.cat-tools.service-xbench',
      'catalogs.services.cat-tools.service-dtptools',
      'catalogs.services.cat-tools.service-memsource',
      'catalogs.services.cat-tools.service-lingotek',
      'catalogs.services.cat-tools.service-verifika',
      'catalogs.services.cat-tools.description',
    ]
  }
]

export const CatalogDetailServices = [
  {
    icon: 'retweet.svg',
    showIcon: false,
    name: 'catalogs.services.detailServices.traslationAndLocalization.title',
    description: 'catalogs.services.detailServices.traslationAndLocalization.description',    
  },
  {
    icon: 'check-circle.svg',
    showIcon: false,
    name: 'catalogs.services.detailServices.edition.title',
    description: 'catalogs.services.detailServices.edition.description',    
  },
  {
    icon: 'eye.svg',
    name: 'catalogs.services.detailServices.audiovisual.title',
    showIcon: false,
    description: 'catalogs.services.detailServices.audiovisual.description',    
  },
  {
    icon: 'ruler.svg',
    showIcon: false,
    name: 'catalogs.services.detailServices.desktopPublishing.title',
    description: 'catalogs.services.detailServices.desktopPublishing.description',    
  },
  {
    icon: 'user-circle.svg',
    showIcon: false,
    name: 'catalogs.services.detailServices.customizedServices.title',
    description: 'catalogs.services.detailServices.customizedServices.description',    
  },
  {
    icon: 'training.svg',
    showIcon: false,
    name: 'catalogs.services.detailServices.training.title',
    description: 'catalogs.services.detailServices.training.description',
    secondDescription: 'catalogs.services.detailServices.training.description-2',
  },
]