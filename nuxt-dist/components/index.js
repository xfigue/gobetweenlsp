export const AboutUsFirstSection = () => import('../../components/about-us-first-section.vue' /* webpackChunkName: "components/about-us-first-section" */).then(c => wrapFunctional(c.default || c))
export const CardDetailService = () => import('../../components/card-detail-service.vue' /* webpackChunkName: "components/card-detail-service" */).then(c => wrapFunctional(c.default || c))
export const CardFile = () => import('../../components/card-file.vue' /* webpackChunkName: "components/card-file" */).then(c => wrapFunctional(c.default || c))
export const CardService = () => import('../../components/card-service.vue' /* webpackChunkName: "components/card-service" */).then(c => wrapFunctional(c.default || c))
export const CardStaff = () => import('../../components/card-staff.vue' /* webpackChunkName: "components/card-staff" */).then(c => wrapFunctional(c.default || c))
export const DetailServices = () => import('../../components/detail-services.vue' /* webpackChunkName: "components/detail-services" */).then(c => wrapFunctional(c.default || c))
export const FeaturedContent = () => import('../../components/featured-content.vue' /* webpackChunkName: "components/featured-content" */).then(c => wrapFunctional(c.default || c))
export const FormBeGoBetweener = () => import('../../components/form-be-go-betweener.vue' /* webpackChunkName: "components/form-be-go-betweener" */).then(c => wrapFunctional(c.default || c))
export const FormContact = () => import('../../components/form-contact.vue' /* webpackChunkName: "components/form-contact" */).then(c => wrapFunctional(c.default || c))
export const FormSubscription = () => import('../../components/form-subscription.vue' /* webpackChunkName: "components/form-subscription" */).then(c => wrapFunctional(c.default || c))
export const LangSwitch = () => import('../../components/lang-switch.vue' /* webpackChunkName: "components/lang-switch" */).then(c => wrapFunctional(c.default || c))
export const ListServices = () => import('../../components/list-services.vue' /* webpackChunkName: "components/list-services" */).then(c => wrapFunctional(c.default || c))
export const ManageTranslation = () => import('../../components/manage-translation.vue' /* webpackChunkName: "components/manage-translation" */).then(c => wrapFunctional(c.default || c))
export const NewXFiles = () => import('../../components/new-x-files..vue' /* webpackChunkName: "components/new-x-files" */).then(c => wrapFunctional(c.default || c))
export const OurStaff = () => import('../../components/our-staff.vue' /* webpackChunkName: "components/our-staff" */).then(c => wrapFunctional(c.default || c))
export const Testimony = () => import('../../components/testimony.vue' /* webpackChunkName: "components/testimony" */).then(c => wrapFunctional(c.default || c))
export const XFiles = () => import('../../components/x-files.vue' /* webpackChunkName: "components/x-files" */).then(c => wrapFunctional(c.default || c))

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
