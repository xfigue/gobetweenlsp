# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<AboutUsFirstSection>` | `<about-us-first-section>` (components/about-us-first-section.vue)
- `<CardDetailService>` | `<card-detail-service>` (components/card-detail-service.vue)
- `<CardFile>` | `<card-file>` (components/card-file.vue)
- `<CardService>` | `<card-service>` (components/card-service.vue)
- `<CardStaff>` | `<card-staff>` (components/card-staff.vue)
- `<DetailServices>` | `<detail-services>` (components/detail-services.vue)
- `<FeaturedContent>` | `<featured-content>` (components/featured-content.vue)
- `<FormBeGoBetweener>` | `<form-be-go-betweener>` (components/form-be-go-betweener.vue)
- `<FormContact>` | `<form-contact>` (components/form-contact.vue)
- `<FormSubscription>` | `<form-subscription>` (components/form-subscription.vue)
- `<LangSwitch>` | `<lang-switch>` (components/lang-switch.vue)
- `<ListServices>` | `<list-services>` (components/list-services.vue)
- `<ManageTranslation>` | `<manage-translation>` (components/manage-translation.vue)
- `<NewXFiles>` | `<new-x-files>` (components/new-x-files..vue)
- `<OurStaff>` | `<our-staff>` (components/our-staff.vue)
- `<Testimony>` | `<testimony>` (components/testimony.vue)
- `<XFiles>` | `<x-files>` (components/x-files.vue)
