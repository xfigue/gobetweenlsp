import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _ccefc300 = () => interopDefault(import('../pages/about-us.vue' /* webpackChunkName: "pages/about-us" */))
const _5acb880d = () => interopDefault(import('../pages/contact-us.vue' /* webpackChunkName: "pages/contact-us" */))
const _0498b0a0 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _2b2abc00 = () => interopDefault(import('../pages/services.vue' /* webpackChunkName: "pages/services" */))
const _c27a41a0 = () => interopDefault(import('../pages/x-files.vue' /* webpackChunkName: "pages/x-files" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about-us",
    component: _ccefc300,
    name: "about-us"
  }, {
    path: "/contact-us",
    component: _5acb880d,
    name: "contact-us"
  }, {
    path: "/en",
    component: _0498b0a0,
    name: "index___en"
  }, {
    path: "/es",
    component: _0498b0a0,
    name: "index___es"
  }, {
    path: "/services",
    component: _2b2abc00,
    name: "services"
  }, {
    path: "/x-files",
    component: _c27a41a0,
    name: "x-files"
  }, {
    path: "/en/about-us",
    component: _ccefc300,
    name: "about-us___en"
  }, {
    path: "/en/contact-us",
    component: _5acb880d,
    name: "contact-us___en"
  }, {
    path: "/en/services",
    component: _2b2abc00,
    name: "services___en"
  }, {
    path: "/en/x-files",
    component: _c27a41a0,
    name: "x-files___en"
  }, {
    path: "/es/about-us",
    component: _ccefc300,
    name: "about-us___es"
  }, {
    path: "/es/contact-us",
    component: _5acb880d,
    name: "contact-us___es"
  }, {
    path: "/es/services",
    component: _2b2abc00,
    name: "services___es"
  }, {
    path: "/es/x-files",
    component: _c27a41a0,
    name: "x-files___es"
  }, {
    path: "/",
    component: _0498b0a0,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
