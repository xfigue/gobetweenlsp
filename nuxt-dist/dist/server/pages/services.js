exports.ids = [22,2,4,6,7,11];
exports.modules = {

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/detail-services.vue?vue&type=template&id=b98ec676&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-service services-mt"},[_vm._ssrNode("<div class=\"title d-inline-block mr-5\" data-v-b98ec676>"+_vm._ssrEscape("\n    "+_vm._s(_vm.$t('pages.services.card-detail.title'))+"\n  ")+"</div> "),_c('b-row',{staticClass:"mx-0 px-0 mt-5"},_vm._l((_vm.catalogDetailServices),function(service){return _c('b-col',{key:service.id,staticClass:"p-1",attrs:{"lg":4,"cols":12}},[_c('card-detail-service',{key:service.name,attrs:{"name":service.name,"description":service.description,"second-description":service.secondDescription,"icon":service.icon,"show-icon":service.showIcon}})],1)}),1)],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/detail-services.vue?vue&type=template&id=b98ec676&scoped=true&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// EXTERNAL MODULE: ./helpers/catalogs.ts
var catalogs = __webpack_require__(63);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/detail-services.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let default_1 = class extends external_nuxt_property_decorator_["Vue"] {
  constructor() {
    super(...arguments);
    this.catalogDetailServices = catalogs["a" /* CatalogDetailServices */];
  }

};
default_1 = __decorate([external_nuxt_property_decorator_["Component"]], default_1);
/* harmony default export */ var detail_servicesvue_type_script_lang_ts_ = (default_1);
// CONCATENATED MODULE: ./components/detail-services.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_detail_servicesvue_type_script_lang_ts_ = (detail_servicesvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/detail-services.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(93)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_detail_servicesvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "b98ec676",
  "fffaaf8a"
  
)

/* harmony default export */ var detail_services = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {CardDetailService: __webpack_require__(83).default})


/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/list-services.vue?vue&type=template&id=257b9638&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',_vm._l((_vm.catalogServices),function(service){return _c('card-service',{key:service.name,attrs:{"name":service.name,"subtitle":service.subtitle,"description":service.description,"icon":service.icon,"rotate-icon":service.rotateIcon,"services":service.services}})}),1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/list-services.vue?vue&type=template&id=257b9638&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// EXTERNAL MODULE: ./helpers/catalogs.ts
var catalogs = __webpack_require__(63);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/list-services.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let default_1 = class extends external_nuxt_property_decorator_["Vue"] {
  constructor() {
    super(...arguments);
    this.catalogServices = catalogs["b" /* CatalogServices */];
  }

};
default_1 = __decorate([external_nuxt_property_decorator_["Component"]], default_1);
/* harmony default export */ var list_servicesvue_type_script_lang_ts_ = (default_1);
// CONCATENATED MODULE: ./components/list-services.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_list_servicesvue_type_script_lang_ts_ = (list_servicesvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/list-services.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_list_servicesvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "85919964"
  
)

/* harmony default export */ var list_services = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {CardService: __webpack_require__(84).default})


/***/ }),

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/services.vue?vue&type=template&id=66c42a40&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('featured-content',{attrs:{"src":"/images/services.jpg","img-class":"services","mainPosition":"master-padding-left","title":"pages.services.block.title","text":"pages.services.block.text","icon":"fas fa-arrow-down","backgroundColor":"block-background-gray","fontColor":"font-color-white","logoHome":"display-none"}}),_vm._ssrNode(" "),_c('detail-services'),_vm._ssrNode(" "),_c('list-services')],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/services.vue?vue&type=template&id=66c42a40&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/services.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};


let default_1 = class extends external_nuxt_property_decorator_["Vue"] {
  head({
    $i18n
  }) {
    return {
      title: $i18n.t('pages.services.title')
    };
  }

};
default_1 = __decorate([Object(external_nuxt_property_decorator_["Component"])({
  layout: 'default'
})], default_1);
/* harmony default export */ var servicesvue_type_script_lang_ts_ = (default_1);
// CONCATENATED MODULE: ./pages/services.vue?vue&type=script&lang=ts&
 /* harmony default export */ var pages_servicesvue_type_script_lang_ts_ = (servicesvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/services.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_servicesvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "0928ec23"
  
)

/* harmony default export */ var services = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {FeaturedContent: __webpack_require__(57).default,DetailServices: __webpack_require__(102).default,ListServices: __webpack_require__(103).default})


/***/ }),

/***/ 54:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(56);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("ec8a999c", content, true, context)
};

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(54);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 56:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".main-content[data-v-7c3c79e6]{position:relative}.main-content .img-container[data-v-7c3c79e6]{overflow-x:hidden}.main-content .img-container .responsive-image[data-v-7c3c79e6]{width:100%}@media (max-width:575px){.main-content .img-container .responsive-image[data-v-7c3c79e6]{width:180%}.main-content .img-container .responsive-image.home[data-v-7c3c79e6]{width:140%;margin-left:-23%}.main-content .img-container .responsive-image.services[data-v-7c3c79e6]{margin-left:-60%}.main-content .img-container .responsive-image.about-us[data-v-7c3c79e6]{margin-left:-20%;width:130%}.main-content .img-container .responsive-image.contact-us[data-v-7c3c79e6]{margin-left:-66%}.main-content .img-container .responsive-image.x-files[data-v-7c3c79e6]{margin-left:-57%}}.padding-right[data-v-7c3c79e6]{padding-right:100px}@media (max-width:575px){.padding-right[data-v-7c3c79e6]{padding-right:10px}}@media (min-width:576px) and (max-width:767px){.padding-right[data-v-7c3c79e6]{padding-right:40px}}@media (min-width:768px) and (max-width:991px){.padding-right[data-v-7c3c79e6]{padding-right:60px}}@media (min-width:992px) and (max-width:1199px){.padding-right[data-v-7c3c79e6]{padding-right:90px}}.padding-left[data-v-7c3c79e6]{padding-left:100px}@media (max-width:575px){.padding-left[data-v-7c3c79e6]{padding-left:10px}}@media (min-width:576px) and (max-width:767px){.padding-left[data-v-7c3c79e6]{padding-left:40px}}@media (min-width:768px) and (max-width:991px){.padding-left[data-v-7c3c79e6]{padding-left:60px}}@media (min-width:992px) and (max-width:1199px){.padding-left[data-v-7c3c79e6]{padding-left:90px}}.block[data-v-7c3c79e6]{position:absolute}@media (max-width:575px){.block[data-v-7c3c79e6]{width:184px;bottom:-40px}.block.about-us[data-v-7c3c79e6]{width:auto}.block.about-us .block-content[data-v-7c3c79e6]{padding:20px 35px}}@media (min-width:576px) and (max-width:767px){.block[data-v-7c3c79e6]{width:205px;bottom:-40px}.block.home[data-v-7c3c79e6]{margin-left:40px}.block.about-us[data-v-7c3c79e6]{width:auto}}@media (min-width:768px) and (max-width:991px){.block[data-v-7c3c79e6]{width:225px;bottom:-60px}.block.home[data-v-7c3c79e6]{margin-left:60px}.block.about-us[data-v-7c3c79e6]{width:auto}}@media (min-width:992px) and (max-width:1199px){.block[data-v-7c3c79e6]{width:255px;bottom:-60px}.block.home[data-v-7c3c79e6]{margin-left:90px}.block.about-us[data-v-7c3c79e6]{width:auto}}@media (min-width:1200px){.block[data-v-7c3c79e6]{width:429px;bottom:-60px}}.block-content[data-v-7c3c79e6]{overflow:hidden;padding:100px 60px}@media (max-width:575px){.block-content[data-v-7c3c79e6]{padding:32px}}@media (min-width:576px) and (max-width:767px){.block-content[data-v-7c3c79e6]{padding:42px}}@media (min-width:768px) and (max-width:991px){.block-content[data-v-7c3c79e6]{padding:50px}}@media (min-width:992px) and (max-width:1199px){.block-content[data-v-7c3c79e6]{padding:50px}}.block-content .about-us-text[data-v-7c3c79e6]{margin-top:0!important}.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-family:\"Silka Bold\";font-size:2rem}@media (max-width:575px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:18px}}@media (min-width:576px) and (max-width:767px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:20px;line-height:25px}}@media (min-width:768px) and (max-width:991px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:25px;line-height:25px}}@media (min-width:992px) and (max-width:1199px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:25px;line-height:25px}}@media (min-width:1200px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:40px;line-height:40px}}.block-content p[data-v-7c3c79e6]{font-family:\"Silka Light\";line-height:20px;margin-top:25px;margin-bottom:25px;font-size:1rem}@media (max-width:575px){.block-content p[data-v-7c3c79e6]{line-height:12px;font-size:9px;margin-top:9px;margin-bottom:10px}}@media (min-width:576px) and (max-width:767px){.block-content p[data-v-7c3c79e6]{font-size:12px;line-height:14px;margin-top:15px}}@media (min-width:768px) and (max-width:991px){.block-content p[data-v-7c3c79e6]{font-size:15px;line-height:16px;margin-top:20px}}@media (min-width:992px) and (max-width:1199px){.block-content p[data-v-7c3c79e6]{font-size:16px;line-height:18px}}@media (min-width:1200px){.block-content p[data-v-7c3c79e6]{font-size:16px;line-height:18px}}.block-content img[data-v-7c3c79e6]{margin-top:10px}.block-content img.display-none[data-v-7c3c79e6]{display:none!important}@media (max-width:575px){.block-content img[data-v-7c3c79e6]{width:15px;margin-top:0}}@media (min-width:576px) and (max-width:767px){.block-content img[data-v-7c3c79e6]{width:25px}}@media (min-width:768px) and (max-width:991px){.block-content img[data-v-7c3c79e6]{width:25px}}.block-background-gray[data-v-7c3c79e6]{background-color:#222}.block-background-white[data-v-7c3c79e6]{background-color:#fff}.block-background-home[data-v-7c3c79e6]{background-color:#fefcf3}.block-background-light[data-v-7c3c79e6]{background-color:#f4eceb}.font-color-white[data-v-7c3c79e6]{color:#fff}.font-color-blue[data-v-7c3c79e6]{color:#292c7e}.font-color-black[data-v-7c3c79e6]{color:#222}@media (max-width:575px){.right[data-v-7c3c79e6]{right:10px}}@media (min-width:576px) and (max-width:767px){.right[data-v-7c3c79e6]{right:40px}}@media (min-width:768px) and (max-width:991px){.right[data-v-7c3c79e6]{right:60px}}@media (min-width:992px) and (max-width:1199px){.right[data-v-7c3c79e6]{right:90px}}@media (min-width:1200px){.right[data-v-7c3c79e6]{right:100px}}@media (max-width:575px){.left[data-v-7c3c79e6]{left:16px}}@media (min-width:576px) and (max-width:767px){.left[data-v-7c3c79e6]{left:40px}}@media (min-width:768px) and (max-width:991px){.left[data-v-7c3c79e6]{left:60px}}@media (min-width:992px) and (max-width:1199px){.left[data-v-7c3c79e6]{left:90px}}@media (min-width:1200px){.left[data-v-7c3c79e6]{left:100px}}.left .about-us[data-v-7c3c79e6]{min-width:320px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/featured-content.vue?vue&type=template&id=7c3c79e6&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:("main-content d-flex " + _vm.mainPosition)},[_vm._ssrNode("<div class=\"img-container w-100\" data-v-7c3c79e6><img"+(_vm._ssrAttr("src",_vm.src))+" alt=\"Responsive image\""+(_vm._ssrClass(null,("responsive-image " + _vm.imgClass)))+" data-v-7c3c79e6></div> <div"+(_vm._ssrClass(null,("block " + _vm.backgroundColor + " " + _vm.boxPosition + " " + _vm.imgClass)))+" data-v-7c3c79e6><div"+(_vm._ssrClass(null,("block-content " + _vm.fontColor)))+" data-v-7c3c79e6><h2"+(_vm._ssrClass(null,_vm.titleClass ? _vm.titleClass : 'title'))+" data-v-7c3c79e6>"+_vm._ssrEscape("          \n        "+_vm._s(_vm.$t(_vm.title))+"\n      ")+"</h2> <p"+(_vm._ssrClass(null,_vm.textClass ? _vm.textClass : ''))+" data-v-7c3c79e6>"+_vm._ssrEscape(_vm._s(_vm.$t(_vm.text)))+"</p> <a"+(_vm._ssrAttr("href",_vm.link))+(_vm._ssrClass(null,_vm.fontColor))+" data-v-7c3c79e6><i"+(_vm._ssrClass(null,_vm.icon))+" data-v-7c3c79e6></i></a> <img"+(_vm._ssrAttr("src",__webpack_require__(20)))+" width=\"43\""+(_vm._ssrClass("mt-1",_vm.logoHome))+" data-v-7c3c79e6></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/featured-content.vue?vue&type=template&id=7c3c79e6&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/featured-content.vue?vue&type=script&lang=ts&
/* harmony default export */ var featured_contentvue_type_script_lang_ts_ = ({
  props: {
    mainPosition: String,
    title: String,
    titleClass: String,
    icon: String,
    text: String,
    textClass: String,
    src: String,
    imgClass: String,
    backgroundColor: String,
    fontColor: String,
    boxPosition: String,
    logoHome: String,
    link: String
  }
});
// CONCATENATED MODULE: ./components/featured-content.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_featured_contentvue_type_script_lang_ts_ = (featured_contentvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/featured-content.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(55)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_featured_contentvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "7c3c79e6",
  "fb337f6e"
  
)

/* harmony default export */ var featured_content = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 61:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(76);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("3faf0b68", content, true, context)
};

/***/ }),

/***/ 62:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(78);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("13fed5b6", content, true, context)
};

/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CatalogServices; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CatalogDetailServices; });
const CatalogServices = [{
  name: 'catalogs.services.field-locale.title',
  description: 'catalogs.services.field-locale.description',
  subtitle: 'catalogs.services.field-locale.subtitle',
  icon: 'fas fa-crosshairs',
  rotateIcon: false,
  services: ['catalogs.services.field-locale.service-technology', 'catalogs.services.field-locale.service-telecommunications', 'catalogs.services.field-locale.service-software', 'catalogs.services.field-locale.service-life-sciences', 'catalogs.services.field-locale.service-gaming', 'catalogs.services.field-locale.service-legal', 'catalogs.services.field-locale.service-education', 'catalogs.services.field-locale.service-travel', 'catalogs.services.field-locale.service-automotive', 'catalogs.services.field-locale.service-more']
}, {
  name: 'catalogs.services.cat-tools.title',
  description: 'catalogs.services.cat-tools.description',
  icon: 'fas fa-wrench',
  rotateIcon: true,
  services: ['catalogs.services.cat-tools.service-linguistic', 'catalogs.services.cat-tools.service-catalyst', 'catalogs.services.cat-tools.service-across', 'catalogs.services.cat-tools.service-trados-studio', 'catalogs.services.cat-tools.service-translation', 'catalogs.services.cat-tools.service-smartling', 'catalogs.services.cat-tools.service-passolo', 'catalogs.services.cat-tools.service-wordfast', 'catalogs.services.cat-tools.service-memoQ', 'catalogs.services.cat-tools.service-xtm', 'catalogs.services.cat-tools.service-xbench', 'catalogs.services.cat-tools.service-dtptools', 'catalogs.services.cat-tools.service-memsource', 'catalogs.services.cat-tools.service-lingotek', 'catalogs.services.cat-tools.service-verifika', 'catalogs.services.cat-tools.description']
}];
const CatalogDetailServices = [{
  icon: 'retweet.svg',
  showIcon: false,
  name: 'catalogs.services.detailServices.traslationAndLocalization.title',
  description: 'catalogs.services.detailServices.traslationAndLocalization.description'
}, {
  icon: 'check-circle.svg',
  showIcon: false,
  name: 'catalogs.services.detailServices.edition.title',
  description: 'catalogs.services.detailServices.edition.description'
}, {
  icon: 'eye.svg',
  name: 'catalogs.services.detailServices.audiovisual.title',
  showIcon: false,
  description: 'catalogs.services.detailServices.audiovisual.description'
}, {
  icon: 'ruler.svg',
  showIcon: false,
  name: 'catalogs.services.detailServices.desktopPublishing.title',
  description: 'catalogs.services.detailServices.desktopPublishing.description'
}, {
  icon: 'user-circle.svg',
  showIcon: false,
  name: 'catalogs.services.detailServices.customizedServices.title',
  description: 'catalogs.services.detailServices.customizedServices.description'
}, {
  icon: 'training.svg',
  showIcon: false,
  name: 'catalogs.services.detailServices.training.title',
  description: 'catalogs.services.detailServices.training.description',
  secondDescription: 'catalogs.services.detailServices.training.description-2'
}];

/***/ }),

/***/ 74:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(94);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("1f8bc3f6", content, true, context)
};

/***/ }),

/***/ 75:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_detail_service_vue_vue_type_style_index_0_id_741a522a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(61);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_detail_service_vue_vue_type_style_index_0_id_741a522a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_detail_service_vue_vue_type_style_index_0_id_741a522a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_detail_service_vue_vue_type_style_index_0_id_741a522a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_detail_service_vue_vue_type_style_index_0_id_741a522a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 76:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".card-detail[data-v-741a522a]{background-color:#f4eceb;min-height:355px}@media (max-width:576px){.card-detail[data-v-741a522a]{padding:18px;min-height:0}}@media (min-width:576px) and (max-width:767px){.card-detail[data-v-741a522a]{padding:26px;min-height:0}}@media (min-width:768px) and (max-width:991px){.card-detail[data-v-741a522a]{padding:32px;min-height:0}}@media (min-width:992px){.card-detail[data-v-741a522a]{padding:55px}}.card-detail .title[data-v-741a522a]{font-family:\"Silka SemiBold\"!important;font-size:16px;line-height:22px;vertical-align:top}@media (max-width:576px){.card-detail .title[data-v-741a522a]{font-size:10px;line-height:10px}}.card-detail .title i[data-v-741a522a]{color:#ea0029;font-size:20px;line-height:20px}@media (max-width:576px){.card-detail .title i[data-v-741a522a]{font-size:10px;line-height:10px}}@media (min-width:576px) and (max-width:767px){.card-detail .title i[data-v-741a522a]{margin-right:15px!important}}.card-detail .description[data-v-741a522a]{font-family:\"Silka Light\"!important;font-size:14px;line-height:20px}@media (max-width:576px){.card-detail .description[data-v-741a522a]{font-size:10px;line-height:10px;margin-top:17px!important;margin-left:39px;margin-bottom:0}}@media (min-width:992px){.card-detail .description[data-v-741a522a]{height:100%;font-size:14px;margin-left:40px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 77:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_service_vue_vue_type_style_index_0_id_388f1e00_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(62);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_service_vue_vue_type_style_index_0_id_388f1e00_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_service_vue_vue_type_style_index_0_id_388f1e00_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_service_vue_vue_type_style_index_0_id_388f1e00_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_service_vue_vue_type_style_index_0_id_388f1e00_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 78:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".container__service[data-v-388f1e00]{margin-top:40px}@media (max-width:576px){.container__service[data-v-388f1e00]{padding:0 30px}}@media (min-width:576px) and (max-width:767px){.container__service[data-v-388f1e00]{padding:0 40px}}@media (min-width:768px) and (max-width:991px){.container__service[data-v-388f1e00]{padding:0 60px}}@media (min-width:992px) and (max-width:1199px){.container__service[data-v-388f1e00]{padding:0 90px}}@media (min-width:1200px){.container__service[data-v-388f1e00]{margin-top:96px;padding:0 100px}}.container__service .title[data-v-388f1e00]{font-family:\"Silka SemiBold\"!important;font-size:25px;line-height:25px}@media (max-width:576px){.container__service .title[data-v-388f1e00]{font-size:20px;line-height:20px}}.container__service .sub-title[data-v-388f1e00]{font-family:\"Silka Light\"!important;font-size:15px;line-height:25px;vertical-align:top}@media (max-width:576px){.container__service .sub-title[data-v-388f1e00]{font-size:10px;line-height:10px;margin-top:13px}}.container__service i[data-v-388f1e00]{color:#ea0029;font-size:15px;vertical-align:top;padding-top:4px;line-height:20px}.container__service i.rotate-icon[data-v-388f1e00]{transform:rotate(-90deg)}.container__service .service__detail[data-v-388f1e00]{font-family:\"Silka\";font-size:16px;line-height:16px}@media (max-width:576px){.container__service .service__detail[data-v-388f1e00]{font-size:10px;line-height:10px}}.container__service .service__detail[data-v-388f1e00]:before{content:\"\\a\";margin-left:10px;margin-right:10px;width:3px;height:3px;vertical-align:middle;border-radius:50%;background:#222;display:inline-block}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/card-detail-service.vue?vue&type=template&id=741a522a&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('b-container',{staticClass:"card-detail"},[_c('div',[_c('div',{staticClass:"title"},[(_vm.showIcon)?_c('i',{class:(_vm.icon + " mr-md-3")}):_vm._e(),_vm._v(" "),(!_vm.showIcon)?_c('img',{attrs:{"width":"20","src":("/images/" + _vm.icon)}}):_vm._e(),_vm._v(" "),_c('label',{staticClass:"ml-3 mb-0"},[_vm._v("\n        "+_vm._s(_vm.$t(_vm.name))+" \n      ")])]),_vm._v(" "),_c('p',{staticClass:"description mt-4"},[_vm._v(" \n      "+_vm._s(_vm.$t(_vm.description))+"\n      "),(_vm.secondDescription)?_c('br'):_vm._e(),_vm._v("\n      "+_vm._s(_vm.secondDescription ? _vm.$t(_vm.secondDescription) : '')+"\n    ")])])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/card-detail-service.vue?vue&type=template&id=741a522a&scoped=true&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/card-detail-service.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};


let card_detail_servicevue_type_script_lang_ts_default_1 = class default_1 extends external_nuxt_property_decorator_["Vue"] {};

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_detail_servicevue_type_script_lang_ts_default_1.prototype, "icon", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_detail_servicevue_type_script_lang_ts_default_1.prototype, "name", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_detail_servicevue_type_script_lang_ts_default_1.prototype, "description", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_detail_servicevue_type_script_lang_ts_default_1.prototype, "secondDescription", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: true
})], card_detail_servicevue_type_script_lang_ts_default_1.prototype, "showIcon", void 0);

card_detail_servicevue_type_script_lang_ts_default_1 = __decorate([external_nuxt_property_decorator_["Component"]], card_detail_servicevue_type_script_lang_ts_default_1);
/* harmony default export */ var card_detail_servicevue_type_script_lang_ts_ = (card_detail_servicevue_type_script_lang_ts_default_1);
// CONCATENATED MODULE: ./components/card-detail-service.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_card_detail_servicevue_type_script_lang_ts_ = (card_detail_servicevue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/card-detail-service.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(75)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_card_detail_servicevue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "741a522a",
  "0564a8f7"
  
)

/* harmony default export */ var card_detail_service = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 84:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/card-service.vue?vue&type=template&id=388f1e00&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container__service pt-5"},[_vm._ssrNode("<div class=\"title d-flex align-items-center mr-5\" data-v-388f1e00><i"+(_vm._ssrClass(null,(_vm.icon + " " + (_vm.rotateIcon ? 'rotate-icon' : '') + " mr-2")))+" data-v-388f1e00></i><label class=\"mb-0\" data-v-388f1e00>"+_vm._ssrEscape(_vm._s(_vm.$t(_vm.name)))+"</label></div> <div class=\"ml-4 sub-title d-inline-block\" data-v-388f1e00>"+_vm._ssrEscape("\n    "+_vm._s(_vm.$t(_vm.description))+"\n    ")+((_vm.subtitle)?("<br data-v-388f1e00>"):"<!---->")+_vm._ssrEscape("\n    "+_vm._s(_vm.subtitle ? _vm.$t(_vm.subtitle) : '')+"\n  ")+"</div> <div class=\"row mx-0 px-0 mt-4\" data-v-388f1e00>"+(_vm._ssrList((_vm.services.filter(function (x) { return _vm.$t(x).length !== 0; })),function(service){return ("<div class=\"col-12 col-sm-6 pl-md-4 col-md-3 py-1 service__detail px-0\" data-v-388f1e00>"+_vm._ssrEscape("\n      "+_vm._s(_vm.$t(service))+"\n    ")+"</div>")}))+"</div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/card-service.vue?vue&type=template&id=388f1e00&scoped=true&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/card-service.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};


let card_servicevue_type_script_lang_ts_default_1 = class default_1 extends external_nuxt_property_decorator_["Vue"] {};

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_servicevue_type_script_lang_ts_default_1.prototype, "name", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_servicevue_type_script_lang_ts_default_1.prototype, "subtitle", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_servicevue_type_script_lang_ts_default_1.prototype, "description", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_servicevue_type_script_lang_ts_default_1.prototype, "icon", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: false
})], card_servicevue_type_script_lang_ts_default_1.prototype, "rotateIcon", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: () => []
})], card_servicevue_type_script_lang_ts_default_1.prototype, "services", void 0);

card_servicevue_type_script_lang_ts_default_1 = __decorate([external_nuxt_property_decorator_["Component"]], card_servicevue_type_script_lang_ts_default_1);
/* harmony default export */ var card_servicevue_type_script_lang_ts_ = (card_servicevue_type_script_lang_ts_default_1);
// CONCATENATED MODULE: ./components/card-service.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_card_servicevue_type_script_lang_ts_ = (card_servicevue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/card-service.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(77)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_card_servicevue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "388f1e00",
  "5de9abe7"
  
)

/* harmony default export */ var card_service = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_detail_services_vue_vue_type_style_index_0_id_b98ec676_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(74);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_detail_services_vue_vue_type_style_index_0_id_b98ec676_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_detail_services_vue_vue_type_style_index_0_id_b98ec676_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_detail_services_vue_vue_type_style_index_0_id_b98ec676_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_detail_services_vue_vue_type_style_index_0_id_b98ec676_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 94:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "@media (max-width:576px){.container-service[data-v-b98ec676]{padding:0 18px!important}}@media (min-width:576px) and (max-width:767px){.container-service[data-v-b98ec676]{padding:0 40px}}@media (min-width:768px) and (max-width:991px){.container-service[data-v-b98ec676]{padding:0 60px}}@media (min-width:992px) and (max-width:1199px){.container-service[data-v-b98ec676]{padding:0 90px}}@media (min-width:1200px){.container-service[data-v-b98ec676]{padding:0 100px}}.services-mt[data-v-b98ec676]{margin-top:110px;margin-bottom:120px}@media (min-width:176px) and (max-width:767px){.services-mt[data-v-b98ec676]{margin-top:80px;margin-bottom:40px}}@media (min-width:768px) and (max-width:992px){.services-mt[data-v-b98ec676]{margin-top:110px;margin-bottom:40px}}.title[data-v-b98ec676]{font-family:\"Silka SemiBold\"!important;font-size:25px;line-height:25px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=services.js.map