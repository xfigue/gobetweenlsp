exports.ids = [21,7,10,12,15,16];
exports.modules = {

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/testimony.vue?vue&type=template&id=1eace82d&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"testimony mt-5 mt-md-0"},[_vm._ssrNode("<h2 class=\"mb-md-5 mb-4 px-2 page-title\">"+_vm._ssrEscape(_vm._s(_vm.$t('components.testimony.title')))+"</h2> "),_c('b-row',{staticClass:"w-100"},[_c('b-col',{staticClass:"pb-5 pb-md-0",attrs:{"md":"12"}},[_c('b-carousel',{attrs:{"id":"carousel-1","interval":10000,"controls":"","indicators":""},on:{"sliding-start":_vm.onSlideStart,"sliding-end":_vm.onSlideEnd},model:{value:(_vm.slide),callback:function ($$v) {_vm.slide=$$v},expression:"slide"}},_vm._l((_vm.testimonyList),function(testimony,index){return _c('b-carousel-slide',{key:index},[_c('div',{staticClass:"text-block"},[_c('p',{staticClass:"text"},[_vm._v("\n              "+_vm._s(_vm.$t(testimony.text))+"\n            ")]),_vm._v(" "),_c('p',{staticClass:"signature mt-5"},[_vm._v(_vm._s(testimony.signature))])])])}),1)],1)],1)],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/testimony.vue?vue&type=template&id=1eace82d&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/testimony.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var testimonyvue_type_script_lang_js_ = ({
  data() {
    return {
      slide: 0,
      sliding: null,
      testimonyList: [{
        text: 'pages.home.testimony-1',
        signature: 'Alejandra Pizarro - Translation Line Manager'
      }, {
        text: 'pages.home.testimony-2',
        signature: 'Alejandra Andreu - Directora de producción'
      }, {
        text: 'pages.home.testimony-3',
        signature: 'Anabela Hotian - Localization Project Manager'
      }, {
        text: 'pages.home.testimony-4',
        signature: 'Virginia Reggiardo - Traductora'
      }, {
        text: 'pages.home.testimony-5',
        signature: 'Roberta Wersio - Traductora'
      }, {
        text: 'pages.home.testimony-6',
        signature: 'Yamila Razzetto - Translator'
      }]
    };
  },

  methods: {
    onSlideStart(slide) {
      this.sliding = true;
    },

    onSlideEnd(slide) {
      this.sliding = false;
    }

  }
});
// CONCATENATED MODULE: ./components/testimony.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_testimonyvue_type_script_lang_js_ = (testimonyvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/testimony.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_testimonyvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "47088d25"
  
)

/* harmony default export */ var testimony = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/manage-translation.vue?vue&type=template&id=7dd59304&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container__manage__translations"},[_vm._ssrNode("<div class=\"row\" data-v-7dd59304><div class=\"col-md-12 col-lg-6 first-column\" data-v-7dd59304><div class=\"last-title title text-white\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.title')))+"</div> <p class=\"text-white hide-md\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.first-description')))+"</p> <p class=\"text-white hide-md\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.second-description')))+"</p> <i class=\"fas fa-arrow-down text-white d-block hide-md\" data-v-7dd59304></i> <a href=\"/files/gobetween_institucional.pdf\" download type=\"button\" class=\"btn bg-white mt-5 px-5 py-3 rounded-0 hide-md text-uppercase\" data-v-7dd59304><i class=\"fas fa-search mr-2\" data-v-7dd59304></i>"+_vm._ssrEscape("\n        "+_vm._s(_vm.$t('components.manage-translation.find-more-about-us'))+"\n      ")+"</a></div> <div class=\"col-md-12 col-lg-6 last-column\" data-v-7dd59304><div class=\"row mx-0 px-0\" data-v-7dd59304><div class=\"col-6\" data-v-7dd59304><div class=\"item-title\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.expertise-title')))+"</div> <div class=\"item-description pr-3 pr-md-0 mt-3\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.expertise-description')))+"</div></div> <div class=\"col-6\" data-v-7dd59304><div class=\"item-title\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.quality-title')))+"</div> <div class=\"item-description pr-3 pr-md-0 mt-3\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.quality-description')))+"</div></div> <div class=\"col-6 mt-5\" data-v-7dd59304><div class=\"item-title\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.availability-title')))+"</div> <div class=\"item-description pr-3 pr-md-0 mt-3\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.availability-description')))+"</div></div> <div class=\"col-6 mt-5\" data-v-7dd59304><div class=\"item-title\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.competitive-rates-title')))+"</div> <div class=\"item-description pr-3 pr-md-0 mt-3\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.competitive-rates-description')))+"</div></div></div></div> <div class=\"col-12 mt-5 show-md\" data-v-7dd59304><p class=\"text-white last-title\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.second-description')))+"</p> <i class=\"fas fa-arrow-down text-white d-block\" data-v-7dd59304></i> <a href=\"/files/gobetween_institucional.pdf\" download class=\"btn bg-white mt-5 px-3 py-2 rounded-0 text-uppercase\" data-v-7dd59304><i class=\"fas fa-search mr-2\" data-v-7dd59304></i>"+_vm._ssrEscape("\n        "+_vm._s(_vm.$t('components.manage-translation.find-more-about-us'))+"\n      ")+"</a></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/manage-translation.vue?vue&type=template&id=7dd59304&scoped=true&

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/manage-translation.vue

var script = {}
function injectStyles (context) {
  
  var style0 = __webpack_require__(91)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  script,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "7dd59304",
  "72c85b78"
  
)

/* harmony default export */ var manage_translation = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/index.vue?vue&type=template&id=7a04b548&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"pr-0"},[_c('featured-content',{attrs:{"src":"/images/home.jpg","img-class":"home","title":"pages.index.block.title","box-position":"left","backgroundColor":"block-background-home home","fontColor":"font-color-black"}}),_vm._ssrNode(" "),_c('manage-translation'),_vm._ssrNode(" "),_c('x-files',{attrs:{"titleClass":"mb-md-5 mb-4 px-2 page-title titleClass","short-version":true}}),_vm._ssrNode(" "),_c('testimony'),_vm._ssrNode(" <div class=\"py-5\" data-v-7a04b548></div> "),_c('form-subscription',{staticClass:"mt-5"})],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/index.vue?vue&type=template&id=7a04b548&scoped=true&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/index.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};


let default_1 = class extends external_nuxt_property_decorator_["Vue"] {
  head({
    $i18n
  }) {
    return {
      title: $i18n.t('pages.index.title')
    };
  }

};
default_1 = __decorate([Object(external_nuxt_property_decorator_["Component"])({
  layout: 'default'
})], default_1);
/* harmony default export */ var lib_vue_loader_options_pagesvue_type_script_lang_ts_ = (default_1);
// CONCATENATED MODULE: ./pages/index.vue?vue&type=script&lang=ts&
 /* harmony default export */ var pagesvue_type_script_lang_ts_ = (lib_vue_loader_options_pagesvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/index.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pagesvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "7a04b548",
  "53e75d46"
  
)

/* harmony default export */ var pages = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {FeaturedContent: __webpack_require__(57).default,ManageTranslation: __webpack_require__(106).default,XFiles: __webpack_require__(82).default,Testimony: __webpack_require__(101).default,FormSubscription: __webpack_require__(81).default})


/***/ }),

/***/ 54:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(56);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("ec8a999c", content, true, context)
};

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(54);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 56:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".main-content[data-v-7c3c79e6]{position:relative}.main-content .img-container[data-v-7c3c79e6]{overflow-x:hidden}.main-content .img-container .responsive-image[data-v-7c3c79e6]{width:100%}@media (max-width:575px){.main-content .img-container .responsive-image[data-v-7c3c79e6]{width:180%}.main-content .img-container .responsive-image.home[data-v-7c3c79e6]{width:140%;margin-left:-23%}.main-content .img-container .responsive-image.services[data-v-7c3c79e6]{margin-left:-60%}.main-content .img-container .responsive-image.about-us[data-v-7c3c79e6]{margin-left:-20%;width:130%}.main-content .img-container .responsive-image.contact-us[data-v-7c3c79e6]{margin-left:-66%}.main-content .img-container .responsive-image.x-files[data-v-7c3c79e6]{margin-left:-57%}}.padding-right[data-v-7c3c79e6]{padding-right:100px}@media (max-width:575px){.padding-right[data-v-7c3c79e6]{padding-right:10px}}@media (min-width:576px) and (max-width:767px){.padding-right[data-v-7c3c79e6]{padding-right:40px}}@media (min-width:768px) and (max-width:991px){.padding-right[data-v-7c3c79e6]{padding-right:60px}}@media (min-width:992px) and (max-width:1199px){.padding-right[data-v-7c3c79e6]{padding-right:90px}}.padding-left[data-v-7c3c79e6]{padding-left:100px}@media (max-width:575px){.padding-left[data-v-7c3c79e6]{padding-left:10px}}@media (min-width:576px) and (max-width:767px){.padding-left[data-v-7c3c79e6]{padding-left:40px}}@media (min-width:768px) and (max-width:991px){.padding-left[data-v-7c3c79e6]{padding-left:60px}}@media (min-width:992px) and (max-width:1199px){.padding-left[data-v-7c3c79e6]{padding-left:90px}}.block[data-v-7c3c79e6]{position:absolute}@media (max-width:575px){.block[data-v-7c3c79e6]{width:184px;bottom:-40px}.block.about-us[data-v-7c3c79e6]{width:auto}.block.about-us .block-content[data-v-7c3c79e6]{padding:20px 35px}}@media (min-width:576px) and (max-width:767px){.block[data-v-7c3c79e6]{width:205px;bottom:-40px}.block.home[data-v-7c3c79e6]{margin-left:40px}.block.about-us[data-v-7c3c79e6]{width:auto}}@media (min-width:768px) and (max-width:991px){.block[data-v-7c3c79e6]{width:225px;bottom:-60px}.block.home[data-v-7c3c79e6]{margin-left:60px}.block.about-us[data-v-7c3c79e6]{width:auto}}@media (min-width:992px) and (max-width:1199px){.block[data-v-7c3c79e6]{width:255px;bottom:-60px}.block.home[data-v-7c3c79e6]{margin-left:90px}.block.about-us[data-v-7c3c79e6]{width:auto}}@media (min-width:1200px){.block[data-v-7c3c79e6]{width:429px;bottom:-60px}}.block-content[data-v-7c3c79e6]{overflow:hidden;padding:100px 60px}@media (max-width:575px){.block-content[data-v-7c3c79e6]{padding:32px}}@media (min-width:576px) and (max-width:767px){.block-content[data-v-7c3c79e6]{padding:42px}}@media (min-width:768px) and (max-width:991px){.block-content[data-v-7c3c79e6]{padding:50px}}@media (min-width:992px) and (max-width:1199px){.block-content[data-v-7c3c79e6]{padding:50px}}.block-content .about-us-text[data-v-7c3c79e6]{margin-top:0!important}.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-family:\"Silka Bold\";font-size:2rem}@media (max-width:575px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:18px}}@media (min-width:576px) and (max-width:767px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:20px;line-height:25px}}@media (min-width:768px) and (max-width:991px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:25px;line-height:25px}}@media (min-width:992px) and (max-width:1199px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:25px;line-height:25px}}@media (min-width:1200px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:40px;line-height:40px}}.block-content p[data-v-7c3c79e6]{font-family:\"Silka Light\";line-height:20px;margin-top:25px;margin-bottom:25px;font-size:1rem}@media (max-width:575px){.block-content p[data-v-7c3c79e6]{line-height:12px;font-size:9px;margin-top:9px;margin-bottom:10px}}@media (min-width:576px) and (max-width:767px){.block-content p[data-v-7c3c79e6]{font-size:12px;line-height:14px;margin-top:15px}}@media (min-width:768px) and (max-width:991px){.block-content p[data-v-7c3c79e6]{font-size:15px;line-height:16px;margin-top:20px}}@media (min-width:992px) and (max-width:1199px){.block-content p[data-v-7c3c79e6]{font-size:16px;line-height:18px}}@media (min-width:1200px){.block-content p[data-v-7c3c79e6]{font-size:16px;line-height:18px}}.block-content img[data-v-7c3c79e6]{margin-top:10px}.block-content img.display-none[data-v-7c3c79e6]{display:none!important}@media (max-width:575px){.block-content img[data-v-7c3c79e6]{width:15px;margin-top:0}}@media (min-width:576px) and (max-width:767px){.block-content img[data-v-7c3c79e6]{width:25px}}@media (min-width:768px) and (max-width:991px){.block-content img[data-v-7c3c79e6]{width:25px}}.block-background-gray[data-v-7c3c79e6]{background-color:#222}.block-background-white[data-v-7c3c79e6]{background-color:#fff}.block-background-home[data-v-7c3c79e6]{background-color:#fefcf3}.block-background-light[data-v-7c3c79e6]{background-color:#f4eceb}.font-color-white[data-v-7c3c79e6]{color:#fff}.font-color-blue[data-v-7c3c79e6]{color:#292c7e}.font-color-black[data-v-7c3c79e6]{color:#222}@media (max-width:575px){.right[data-v-7c3c79e6]{right:10px}}@media (min-width:576px) and (max-width:767px){.right[data-v-7c3c79e6]{right:40px}}@media (min-width:768px) and (max-width:991px){.right[data-v-7c3c79e6]{right:60px}}@media (min-width:992px) and (max-width:1199px){.right[data-v-7c3c79e6]{right:90px}}@media (min-width:1200px){.right[data-v-7c3c79e6]{right:100px}}@media (max-width:575px){.left[data-v-7c3c79e6]{left:16px}}@media (min-width:576px) and (max-width:767px){.left[data-v-7c3c79e6]{left:40px}}@media (min-width:768px) and (max-width:991px){.left[data-v-7c3c79e6]{left:60px}}@media (min-width:992px) and (max-width:1199px){.left[data-v-7c3c79e6]{left:90px}}@media (min-width:1200px){.left[data-v-7c3c79e6]{left:100px}}.left .about-us[data-v-7c3c79e6]{min-width:320px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/featured-content.vue?vue&type=template&id=7c3c79e6&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:("main-content d-flex " + _vm.mainPosition)},[_vm._ssrNode("<div class=\"img-container w-100\" data-v-7c3c79e6><img"+(_vm._ssrAttr("src",_vm.src))+" alt=\"Responsive image\""+(_vm._ssrClass(null,("responsive-image " + _vm.imgClass)))+" data-v-7c3c79e6></div> <div"+(_vm._ssrClass(null,("block " + _vm.backgroundColor + " " + _vm.boxPosition + " " + _vm.imgClass)))+" data-v-7c3c79e6><div"+(_vm._ssrClass(null,("block-content " + _vm.fontColor)))+" data-v-7c3c79e6><h2"+(_vm._ssrClass(null,_vm.titleClass ? _vm.titleClass : 'title'))+" data-v-7c3c79e6>"+_vm._ssrEscape("          \n        "+_vm._s(_vm.$t(_vm.title))+"\n      ")+"</h2> <p"+(_vm._ssrClass(null,_vm.textClass ? _vm.textClass : ''))+" data-v-7c3c79e6>"+_vm._ssrEscape(_vm._s(_vm.$t(_vm.text)))+"</p> <a"+(_vm._ssrAttr("href",_vm.link))+(_vm._ssrClass(null,_vm.fontColor))+" data-v-7c3c79e6><i"+(_vm._ssrClass(null,_vm.icon))+" data-v-7c3c79e6></i></a> <img"+(_vm._ssrAttr("src",__webpack_require__(20)))+" width=\"43\""+(_vm._ssrClass("mt-1",_vm.logoHome))+" data-v-7c3c79e6></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/featured-content.vue?vue&type=template&id=7c3c79e6&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/featured-content.vue?vue&type=script&lang=ts&
/* harmony default export */ var featured_contentvue_type_script_lang_ts_ = ({
  props: {
    mainPosition: String,
    title: String,
    titleClass: String,
    icon: String,
    text: String,
    textClass: String,
    src: String,
    imgClass: String,
    backgroundColor: String,
    fontColor: String,
    boxPosition: String,
    logoHome: String,
    link: String
  }
});
// CONCATENATED MODULE: ./components/featured-content.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_featured_contentvue_type_script_lang_ts_ = (featured_contentvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/featured-content.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(55)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_featured_contentvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "7c3c79e6",
  "fb337f6e"
  
)

/* harmony default export */ var featured_content = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 59:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(68);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("417d296e", content, true, context)
};

/***/ }),

/***/ 60:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(73);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("16f8e6d4", content, true, context)
};

/***/ }),

/***/ 67:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_subscription_vue_vue_type_style_index_0_id_30a452e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(59);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_subscription_vue_vue_type_style_index_0_id_30a452e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_subscription_vue_vue_type_style_index_0_id_30a452e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_subscription_vue_vue_type_style_index_0_id_30a452e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_subscription_vue_vue_type_style_index_0_id_30a452e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 68:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".container__form[data-v-30a452e2]{width:1200px;max-width:100%;margin:auto}.container__form .title[data-v-30a452e2]{font-family:\"Silka SemiBold\"!important;font-size:2.3rem}@media (max-width:575px){.container__form .title[data-v-30a452e2]{font-size:20px;line-height:20px}}.container__form .description[data-v-30a452e2]{font-family:\"Silka\";border-color:#222!important;font-size:18px;line-height:20px;font-weight:300;max-width:100%}@media (min-width:900px){.container__form .description[data-v-30a452e2]{width:440px}}@media (max-width:575px){.container__form .description[data-v-30a452e2]{font-size:10px}}.container__form .container__input[data-v-30a452e2]{width:calc(100% - 106px)}@media (max-width:575px){.container__form .form-control[data-v-30a452e2]{font-size:10px}}@media (max-width:575px){.container__form .btn-submit[data-v-30a452e2]{font-size:8px}}.container__form .input-group-text[data-v-30a452e2]{padding-left:0}.container__form .form-control[data-v-30a452e2]{font-family:\"Silka\"}.container__form button[data-v-30a452e2]{margin-top:5px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 71:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(92);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("623f5c3a", content, true, context)
};

/***/ }),

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_x_files_vue_vue_type_style_index_0_id_211a7261_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(60);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_x_files_vue_vue_type_style_index_0_id_211a7261_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_x_files_vue_vue_type_style_index_0_id_211a7261_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_x_files_vue_vue_type_style_index_0_id_211a7261_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_x_files_vue_vue_type_style_index_0_id_211a7261_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 73:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".container-xfiles[data-v-211a7261]{padding:0!important;max-width:100%;margin:50px auto 0}@media (max-width:575px){.container-xfiles[data-v-211a7261]{margin:80px auto 0}.container-xfiles .page-title[data-v-211a7261]{font-size:20px;margin-bottom:2em!important}}@media (min-width:576px) and (max-width:767px){.container-xfiles[data-v-211a7261]{margin:70px 30px 0}}@media (min-width:768px) and (max-width:991px){.container-xfiles[data-v-211a7261]{margin:100px 45px}}@media (min-width:992px) and (max-width:1199px){.container-xfiles[data-v-211a7261]{margin:100px 80px}}@media (min-width:1200px){.container-xfiles[data-v-211a7261]{margin:100px}}.see-more[data-v-211a7261]{color:#222;margin-right:4px}.see-more span[data-v-211a7261]{font-family:\"Silka Medium\";text-decoration:underline;font-size:15px;letter-spacing:2px}.x-file-card[data-v-211a7261]{margin:0,6rem 0 0;min-height:70px}.x-file-card .image[data-v-211a7261]{width:100%}.titleClass[data-v-211a7261]{font-size:30px!important}.x-file-card .date[data-v-211a7261]{font-family:\"Silka\"!important}@media (max-width:575px){.x-file-card .date[data-v-211a7261]{font-size:8px;line-height:8px}}@media (min-width:1200px){.x-file-card .date[data-v-211a7261]{font-size:15px}}.x-file-card .title[data-v-211a7261]{font-family:\"Silka SemiBold\"!important}@media (max-width:575px){.x-file-card .title[data-v-211a7261]{font-size:10px;line-height:10px}}.overlay-container[data-v-211a7261]{position:relative}.overlay-container:hover .overlay[data-v-211a7261]{opacity:1}.overlay-container .overlay[data-v-211a7261]{position:absolute;display:flex;justify-content:center;align-items:center;color:#fff;top:0;bottom:0;left:0;right:0;height:100%;width:100%;opacity:0;transition:.5s ease;background-color:rgba(34,34,34,.86667)}.overlay-container .overlay .icon[data-v-211a7261]{background-color:#fff;font-size:18px;color:#222;width:48px;height:48px;display:flex;align-items:center;justify-content:center;justify-self:center;border-radius:50%}.overlay-container .overlay a[data-v-211a7261]{font-family:\"Silka\";font-size:15px;margin-top:-15px;color:#fff;font-weight:400;cursor:pointer}.overlay-container .overlay a[data-v-211a7261]:hover{text-decoration:none}.overlay-container .image[data-v-211a7261]{display:block;height:auto}.row[data-v-211a7261]{margin-right:0;margin-left:0;padding:0!important}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 81:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/form-subscription.vue?vue&type=template&id=30a452e2&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container__form pt-md-5"},[_vm._ssrNode("<div class=\"row px-2 px-lg-4\" data-v-30a452e2><div class=\"col-12 col-lg-5\" data-v-30a452e2><div class=\"title text-left text-md-right pr-5 mb-4\" data-v-30a452e2>"+_vm._ssrEscape("\n        "+_vm._s(_vm.$t('components.forms.subscribe.title'))+"\n      ")+"</div></div> <div class=\"col-12 col-md-7 col-lg-6\" data-v-30a452e2><div class=\"description border-left pl-5\" data-v-30a452e2>"+_vm._ssrEscape("\n        "+_vm._s(_vm.$t('components.forms.subscribe.description'))+"\n      ")+"</div> <form id=\"subscription-form\" name=\"subscription-form\" data-v-30a452e2><div class=\"form-group pl-0 pl-md-5 mt-5\" data-v-30a452e2><div class=\"container__input d-inline-block\" data-v-30a452e2><div class=\"input-group mb-2\" data-v-30a452e2><div class=\"input-group-prepend\" data-v-30a452e2><div class=\"input-group-text\" data-v-30a452e2><i class=\"fas fa-arrow-right\" data-v-30a452e2></i></div></div> <input type=\"email\" name=\"email\" id=\"email\""+(_vm._ssrAttr("placeholder",_vm.$t('components.forms.subscribe.email')))+(_vm._ssrAttr("value",(_vm.formModel.email)))+" class=\"form-control\" data-v-30a452e2></div></div> <button type=\"submit\""+(_vm._ssrAttr("disabled",_vm.disabled))+" class=\"btn btn-submit uppercase float-right float-md-none mr-1 text-white d-inline-block\" data-v-30a452e2><i class=\"far fa-paper-plane text-with mr-2\" data-v-30a452e2></i>"+_vm._ssrEscape("\n            "+_vm._s(_vm.$t('components.forms.subscribe.submit'))+"\n          ")+"</button></div></form></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/form-subscription.vue?vue&type=template&id=30a452e2&scoped=true&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// EXTERNAL MODULE: external "emailjs-com"
var external_emailjs_com_ = __webpack_require__(52);
var external_emailjs_com_default = /*#__PURE__*/__webpack_require__.n(external_emailjs_com_);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/form-subscription.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let default_1 = class extends external_nuxt_property_decorator_["Vue"] {
  constructor() {
    super(...arguments);
    this.disabled = false;
    this.formModel = {
      email: '',
      contactType: 'Subscription'
    };
  }

  onSubmit() {
    if (!this.formModel.email) {
      const message = this.$i18n.t('components.forms.subscribe.error-email');
      this.$bvToast.toast(message, {
        title: `Error`,
        variant: 'danger',
        solid: true
      });
    } else {
      this.disabled = true;
      external_emailjs_com_default.a.sendForm(this.$config.emailjsService, this.$config.emailjsContact, 'subscription-form', this.$config.emailjsUserId).then(() => {
        const message = this.$i18n.t('components.forms.subscribe.sended');
        this.$bvToast.toast(message, {
          title: 'gobetween',
          variant: 'success',
          solid: true
        });
        this.formModel = {
          email: '',
          contactType: ''
        };
        this.disabled = false;
      }, error => {
        this.$bvToast.toast(error, {
          title: `Error`,
          variant: 'danger',
          solid: true
        });
        this.disabled = false;
      });
    }
  }

};
default_1 = __decorate([external_nuxt_property_decorator_["Component"]], default_1);
/* harmony default export */ var form_subscriptionvue_type_script_lang_ts_ = (default_1);
// CONCATENATED MODULE: ./components/form-subscription.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_form_subscriptionvue_type_script_lang_ts_ = (form_subscriptionvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/form-subscription.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(67)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_form_subscriptionvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "30a452e2",
  "135e4d27"
  
)

/* harmony default export */ var form_subscription = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 82:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/x-files.vue?vue&type=template&id=211a7261&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-xfiles  xfiles-mt"},[_vm._ssrNode("<div class=\"row d-flex justify-content-between align-items-center\" data-v-211a7261>","</div>",[_vm._ssrNode("<h2"+(_vm._ssrClass(null,_vm.titleClass ? _vm.titleClass : 'mb-md-5 mb-4 px-2 page-title'))+" data-v-211a7261>"+_vm._ssrEscape(_vm._s(_vm.$t('pages.x-files.page-title')))+"</h2> "),(!_vm.isXFiles)?_c('nuxt-link',{staticClass:"see-more mb-2 mt-5 ",attrs:{"to":("/" + (_vm.$i18n.locale) + "/x-files")}},[_c('i',{staticClass:"far fa-eye mr-2"}),_c('span',{staticClass:"d-none d-sm-inline-block"},[_vm._v(_vm._s(_vm.$t('components.x-files.see-all')))])]):_vm._e()],2),_vm._ssrNode(" "),_c('b-row',_vm._l((_vm.files),function(file){return _c('b-col',{key:file.id,staticClass:"x-file-card",attrs:{"lg":3,"cols":6}},[_c('div',{staticClass:"overlay-container"},[_c('img',{staticClass:"image",attrs:{"src":file.imgUrl}}),_vm._v(" "),_c('div',{staticClass:"overlay"},[_c('div',{staticClass:"text-center d-flex flex-column align-items-center"},[_c('i',{staticClass:"icon",class:file.downloadIcon}),_c('br'),_vm._v(" "),_c('a',{attrs:{"href":file.downloadUrl,"target":"_blank","rel":"noopener noreferrer"}},[_vm._v(_vm._s(_vm.$t(file.downloadText)))])])])]),_vm._v(" "),_c('p',{staticClass:"title mt-4 mb-2"},[_vm._v("\n        "+_vm._s(_vm.$t(file.title))+"\n      ")]),_vm._v(" "),_c('p',{staticClass:"date"},[_vm._v("\n        "+_vm._s(file.date)+"\n      ")])])}),1)],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/x-files.vue?vue&type=template&id=211a7261&scoped=true&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/x-files.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};


let x_filesvue_type_script_lang_ts_default_1 = class default_1 extends external_nuxt_property_decorator_["Vue"] {
  constructor() {
    super(...arguments);
    this.isXFiles = this.$route.fullPath.includes('x-files');
    this.items = [{
      id: 1,
      title: 'x-file-1-title',
      imgUrl: '/images/xfile-1.jpg',
      date: '08/01/2021',
      downloadIcon: 'fab fa-google-drive',
      downloadText: 'pages.x-files.download-from-gdrive',
      downloadUrl: 'https://drive.google.com/drive/u/0/folders/1fRnRtyYVRz5dlek8zMb1USMllXn5cnf2'
    }, {
      id: 2,
      title: 'x-file-2-title',
      imgUrl: '/images/xfile-2.jpg',
      date: '08/01/2021',
      downloadIcon: 'fab fa-google-drive',
      downloadText: 'pages.x-files.download-from-gdrive',
      downloadUrl: 'https://drive.google.com/drive/u/0/folders/18XHd6JE4LZ4ud_a3cVBDf6LhX4iW-ZBo'
    }, {
      id: 3,
      title: 'x-file-3-title',
      imgUrl: '/images/xfile-3.jpg',
      date: '08/01/2021',
      downloadIcon: 'fab fa-google-drive',
      downloadText: 'pages.x-files.download-from-gdrive',
      downloadUrl: 'https://drive.google.com/drive/u/0/folders/1pSE92zR1hAUSEsV5bMVsqErJ_xxf0Hce'
    }, {
      id: 4,
      title: 'x-file-4-title',
      imgUrl: '/images/xfile-4.jpg',
      date: '08/01/2021',
      downloadIcon: 'fab fa-google-drive',
      downloadText: 'pages.x-files.download-from-gdrive',
      downloadUrl: 'https://drive.google.com/drive/u/0/folders/1hQEwReghNci3xFbsroUNGt2p23GlbSJh'
    }, {
      id: 5,
      title: 'x-file-5-title',
      imgUrl: '/images/xfile-5.jpg',
      date: '08/01/2021',
      downloadIcon: 'fab fa-google-drive',
      downloadText: 'pages.x-files.download-from-gdrive',
      downloadUrl: 'https://drive.google.com/drive/u/0/folders/1F1fVxu9Y9Ld1uRu5aWqbPSZTeVsDo3qO'
    }, {
      id: 6,
      title: 'x-file-6-title',
      imgUrl: '/images/video1.png',
      date: '15/09/2021',
      downloadIcon: 'fab fa-google-drive',
      downloadText: 'pages.x-files.download-from-gdrive',
      downloadUrl: 'https://drive.google.com/file/d/1nzZuUQPP3xrGXNRoTW3InlFisAvecamF'
    }, {
      id: 7,
      title: 'x-file-7-title',
      imgUrl: '/images/video2.png',
      date: '15/09/2021',
      downloadIcon: 'fab fa-google-drive',
      downloadText: 'pages.x-files.download-from-gdrive',
      downloadUrl: 'https://drive.google.com/file/d/1vNtu_dRD5RcSPv7cmWxt3qK_hc57PBfs'
    }, {
      id: 8,
      title: 'x-file-8-title',
      imgUrl: '/images/video3.png',
      date: '15/09/2021',
      downloadIcon: 'fab fa-google-drive',
      downloadText: 'pages.x-files.download-from-gdrive',
      downloadUrl: 'https://drive.google.com/file/d/1VhnkE1SvcxuDtJa9DJPbsxySoQX70VPd'
    }];
  }

  get files() {
    if (!this.$route.fullPath.includes('x-files')) {
      return this.items.slice(0, 4);
    }

    return this.items;
  }

};

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: 'titleClass'
})], x_filesvue_type_script_lang_ts_default_1.prototype, "titleClass", void 0);

x_filesvue_type_script_lang_ts_default_1 = __decorate([Object(external_nuxt_property_decorator_["Component"])({
  layout: 'default'
})], x_filesvue_type_script_lang_ts_default_1);
/* harmony default export */ var x_filesvue_type_script_lang_ts_ = (x_filesvue_type_script_lang_ts_default_1);
// CONCATENATED MODULE: ./components/x-files.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_x_filesvue_type_script_lang_ts_ = (x_filesvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/x-files.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(72)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_x_filesvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "211a7261",
  "2d53cbc3"
  
)

/* harmony default export */ var x_files = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 91:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_manage_translation_vue_vue_type_style_index_0_id_7dd59304_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(71);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_manage_translation_vue_vue_type_style_index_0_id_7dd59304_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_manage_translation_vue_vue_type_style_index_0_id_7dd59304_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_manage_translation_vue_vue_type_style_index_0_id_7dd59304_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_manage_translation_vue_vue_type_style_index_0_id_7dd59304_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 92:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".container__manage__translations[data-v-7dd59304]{padding:180px 0;background:#ea0029}@media (max-width:992px){.container__manage__translations>.row[data-v-7dd59304]{padding:0}}.container__manage__translations .col-md-12.first-column[data-v-7dd59304]{padding-left:80px}@media (max-width:992px){.container__manage__translations .col-md-12.first-column[data-v-7dd59304]{padding-left:0}}.container__manage__translations .col-md-12.last-column[data-v-7dd59304]{padding-right:80px}@media (max-width:992px){.container__manage__translations .col-md-12.last-column[data-v-7dd59304]{margin-top:50px;padding:0!important}}@media (max-width:992px){.container__manage__translations .col-md-12.last-column .col-6[data-v-7dd59304]{padding:0}}.container__manage__translations .col-md-12 .title[data-v-7dd59304]{font-size:45px;line-height:50px;width:500px;max-width:100%}@media (max-width:992px){.container__manage__translations .col-md-12 .title[data-v-7dd59304]{font-size:20px;line-height:20px;width:280px;max-width:100%}}.container__manage__translations .col-md-12 p[data-v-7dd59304]{font-family:\"Silka\";font-size:16px;line-height:20px;font-weight:300;width:450px;max-width:100%;margin:50px 0}.container__manage__translations .btn[data-v-7dd59304]{font-family:\"Silka\";font-size:15px}.container__manage__translations .btn[data-v-7dd59304]:hover{border:1px solid #000}@media (max-width:992px){.container__manage__translations .btn[data-v-7dd59304]{font-size:13px}}.container__manage__translations .item-title[data-v-7dd59304]{font-family:\"Silka Semibold\";display:flex;height:50px;align-items:start;font-size:20px;line-height:22px;font-weight:500;padding-left:25px;border-left:1px solid #fff;color:#fff;margin-bottom:25px}@media (max-width:992px){.container__manage__translations .item-title[data-v-7dd59304]{font-size:10px;line-height:10px;height:35px}}.container__manage__translations .item-description[data-v-7dd59304]{font-family:\"Silka\";font-size:18px;line-height:20px;font-weight:300;padding-left:26px;color:#fff}@media (max-width:992px){.container__manage__translations .item-description[data-v-7dd59304]{font-size:10px;line-height:10px}}@media (max-width:992px){.container__manage__translations[data-v-7dd59304]{padding:100px 25px 60px}}@media (max-width:575px){.container__manage__translations[data-v-7dd59304]{width:calc(100% - 17px);margin-left:17px}}@media (min-width:576px) and (max-width:767px){.container__manage__translations[data-v-7dd59304]{width:calc(100% - 41px);margin-left:41px}}@media (min-width:768px) and (max-width:991px){.container__manage__translations[data-v-7dd59304]{width:calc(100% - 61px);margin-left:61px}}@media (min-width:992px) and (max-width:1199px){.container__manage__translations[data-v-7dd59304]{width:calc(100% - 91px);margin-left:91px}}@media (min-width:1200px){.container__manage__translations[data-v-7dd59304]{width:calc(100% - 106px);margin-left:101px}}.container__manage__translations .last-title[data-v-7dd59304]{font-family:\"Silka Bold\"}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=index.js.map