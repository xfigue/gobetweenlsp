exports.ids = [19,1,5,7,10,14];
exports.modules = {

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/our-staff.vue?vue&type=template&id=3ce83771&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container__staff"},[_vm._ssrNode("<h2 class=\"mb-md-5 mb-4 px-2 page-title\" data-v-3ce83771>"+_vm._ssrEscape(_vm._s(_vm.$t('pages.about-us.next-generation-of-leaders')))+"</h2> "),_vm._ssrNode("<div class=\"row\" data-v-3ce83771>","</div>",[_vm._ssrNode("<div class=\"col-6 col-sm-3 px-1 px-sm-3\" data-v-3ce83771>","</div>",[_c('card-staff',{attrs:{"avatar":"/images/staff/melina-delpontigo.jpg","name":"Melina Delpontigo","position":"pages.about-us.melina-position","linkedin":"https://www.linkedin.com/in/melina-delpontigo-20b39521/"}})],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-6 col-sm-3 px-1 px-sm-3\" data-v-3ce83771>","</div>",[_c('card-staff',{attrs:{"avatar":"/images/staff/andrea-alvarez.jpg","name":"Andrea Alvarez","position":"pages.about-us.andrea-position","linkedin":"https://www.linkedin.com/in/andrea-alvarez-69a866112/"}})],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-6 col-sm-3 mt-4 mt-sm-0 px-1 px-sm-3\" data-v-3ce83771>","</div>",[_c('card-staff',{attrs:{"avatar":"/images/staff/fiorella-dottori.jpg","name":"Fiorela Dottori","position":"pages.about-us.fiorella-position","linkedin":"https://www.linkedin.com/in/fiorela-dottori-34b701193/"}})],1),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"col-6 col-sm-3 mt-4 mt-sm-0 px-1 px-sm-3\" data-v-3ce83771>","</div>",[_c('card-staff',{attrs:{"avatar":"/images/staff/lucia-torres.jpg","name":"Lucía Torres","position":"pages.about-us.lucia-position","linkedin":"https://www.linkedin.com/in/luc%C3%ADa-torres-39498799/"}})],1)],2)],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/our-staff.vue?vue&type=template&id=3ce83771&scoped=true&

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/our-staff.vue

var script = {}
function injectStyles (context) {
  
  var style0 = __webpack_require__(85)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  script,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "3ce83771",
  "72db7f40"
  
)

/* harmony default export */ var our_staff = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {CardStaff: __webpack_require__(80).default})


/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/about-us.vue?vue&type=template&id=4d8dc95e&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('featured-content',{attrs:{"src":"/images/about-us.jpg","img-class":"about-us","mainPosition":"master-padding-right","title":"pages.about-us.block.title","text":"pages.about-us.block.text","textClass":"about-us-text","icon":"fas fa-arrow-down","boxPosition":"left","backgroundColor":"block-background-home","fontColor":"font-color-black","logoHome":"display-none"}}),_vm._ssrNode(" "),_c('about-us-first-section'),_vm._ssrNode(" "),_c('our-staff'),_vm._ssrNode(" "),_c('form-subscription')],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/about-us.vue?vue&type=template&id=4d8dc95e&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// EXTERNAL MODULE: ./components/about-us-first-section.vue + 2 modules
var about_us_first_section = __webpack_require__(97);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/about-us.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let default_1 = class extends external_nuxt_property_decorator_["Vue"] {
  head({
    $i18n
  }) {
    return {
      title: $i18n.t('pages.about-us.title')
    };
  }

};
default_1 = __decorate([Object(external_nuxt_property_decorator_["Component"])({
  layout: 'default',
  components: {
    AboutUsFirstSection: about_us_first_section["default"]
  }
})], default_1);
/* harmony default export */ var about_usvue_type_script_lang_ts_ = (default_1);
// CONCATENATED MODULE: ./pages/about-us.vue?vue&type=script&lang=ts&
 /* harmony default export */ var pages_about_usvue_type_script_lang_ts_ = (about_usvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/about-us.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_about_usvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "8f732eba"
  
)

/* harmony default export */ var about_us = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {FeaturedContent: __webpack_require__(57).default,AboutUsFirstSection: __webpack_require__(97).default,OurStaff: __webpack_require__(105).default,FormSubscription: __webpack_require__(81).default})


/***/ }),

/***/ 54:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(56);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("ec8a999c", content, true, context)
};

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(54);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 56:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".main-content[data-v-7c3c79e6]{position:relative}.main-content .img-container[data-v-7c3c79e6]{overflow-x:hidden}.main-content .img-container .responsive-image[data-v-7c3c79e6]{width:100%}@media (max-width:575px){.main-content .img-container .responsive-image[data-v-7c3c79e6]{width:180%}.main-content .img-container .responsive-image.home[data-v-7c3c79e6]{width:140%;margin-left:-23%}.main-content .img-container .responsive-image.services[data-v-7c3c79e6]{margin-left:-60%}.main-content .img-container .responsive-image.about-us[data-v-7c3c79e6]{margin-left:-20%;width:130%}.main-content .img-container .responsive-image.contact-us[data-v-7c3c79e6]{margin-left:-66%}.main-content .img-container .responsive-image.x-files[data-v-7c3c79e6]{margin-left:-57%}}.padding-right[data-v-7c3c79e6]{padding-right:100px}@media (max-width:575px){.padding-right[data-v-7c3c79e6]{padding-right:10px}}@media (min-width:576px) and (max-width:767px){.padding-right[data-v-7c3c79e6]{padding-right:40px}}@media (min-width:768px) and (max-width:991px){.padding-right[data-v-7c3c79e6]{padding-right:60px}}@media (min-width:992px) and (max-width:1199px){.padding-right[data-v-7c3c79e6]{padding-right:90px}}.padding-left[data-v-7c3c79e6]{padding-left:100px}@media (max-width:575px){.padding-left[data-v-7c3c79e6]{padding-left:10px}}@media (min-width:576px) and (max-width:767px){.padding-left[data-v-7c3c79e6]{padding-left:40px}}@media (min-width:768px) and (max-width:991px){.padding-left[data-v-7c3c79e6]{padding-left:60px}}@media (min-width:992px) and (max-width:1199px){.padding-left[data-v-7c3c79e6]{padding-left:90px}}.block[data-v-7c3c79e6]{position:absolute}@media (max-width:575px){.block[data-v-7c3c79e6]{width:184px;bottom:-40px}.block.about-us[data-v-7c3c79e6]{width:auto}.block.about-us .block-content[data-v-7c3c79e6]{padding:20px 35px}}@media (min-width:576px) and (max-width:767px){.block[data-v-7c3c79e6]{width:205px;bottom:-40px}.block.home[data-v-7c3c79e6]{margin-left:40px}.block.about-us[data-v-7c3c79e6]{width:auto}}@media (min-width:768px) and (max-width:991px){.block[data-v-7c3c79e6]{width:225px;bottom:-60px}.block.home[data-v-7c3c79e6]{margin-left:60px}.block.about-us[data-v-7c3c79e6]{width:auto}}@media (min-width:992px) and (max-width:1199px){.block[data-v-7c3c79e6]{width:255px;bottom:-60px}.block.home[data-v-7c3c79e6]{margin-left:90px}.block.about-us[data-v-7c3c79e6]{width:auto}}@media (min-width:1200px){.block[data-v-7c3c79e6]{width:429px;bottom:-60px}}.block-content[data-v-7c3c79e6]{overflow:hidden;padding:100px 60px}@media (max-width:575px){.block-content[data-v-7c3c79e6]{padding:32px}}@media (min-width:576px) and (max-width:767px){.block-content[data-v-7c3c79e6]{padding:42px}}@media (min-width:768px) and (max-width:991px){.block-content[data-v-7c3c79e6]{padding:50px}}@media (min-width:992px) and (max-width:1199px){.block-content[data-v-7c3c79e6]{padding:50px}}.block-content .about-us-text[data-v-7c3c79e6]{margin-top:0!important}.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-family:\"Silka Bold\";font-size:2rem}@media (max-width:575px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:18px}}@media (min-width:576px) and (max-width:767px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:20px;line-height:25px}}@media (min-width:768px) and (max-width:991px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:25px;line-height:25px}}@media (min-width:992px) and (max-width:1199px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:25px;line-height:25px}}@media (min-width:1200px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:40px;line-height:40px}}.block-content p[data-v-7c3c79e6]{font-family:\"Silka Light\";line-height:20px;margin-top:25px;margin-bottom:25px;font-size:1rem}@media (max-width:575px){.block-content p[data-v-7c3c79e6]{line-height:12px;font-size:9px;margin-top:9px;margin-bottom:10px}}@media (min-width:576px) and (max-width:767px){.block-content p[data-v-7c3c79e6]{font-size:12px;line-height:14px;margin-top:15px}}@media (min-width:768px) and (max-width:991px){.block-content p[data-v-7c3c79e6]{font-size:15px;line-height:16px;margin-top:20px}}@media (min-width:992px) and (max-width:1199px){.block-content p[data-v-7c3c79e6]{font-size:16px;line-height:18px}}@media (min-width:1200px){.block-content p[data-v-7c3c79e6]{font-size:16px;line-height:18px}}.block-content img[data-v-7c3c79e6]{margin-top:10px}.block-content img.display-none[data-v-7c3c79e6]{display:none!important}@media (max-width:575px){.block-content img[data-v-7c3c79e6]{width:15px;margin-top:0}}@media (min-width:576px) and (max-width:767px){.block-content img[data-v-7c3c79e6]{width:25px}}@media (min-width:768px) and (max-width:991px){.block-content img[data-v-7c3c79e6]{width:25px}}.block-background-gray[data-v-7c3c79e6]{background-color:#222}.block-background-white[data-v-7c3c79e6]{background-color:#fff}.block-background-home[data-v-7c3c79e6]{background-color:#fefcf3}.block-background-light[data-v-7c3c79e6]{background-color:#f4eceb}.font-color-white[data-v-7c3c79e6]{color:#fff}.font-color-blue[data-v-7c3c79e6]{color:#292c7e}.font-color-black[data-v-7c3c79e6]{color:#222}@media (max-width:575px){.right[data-v-7c3c79e6]{right:10px}}@media (min-width:576px) and (max-width:767px){.right[data-v-7c3c79e6]{right:40px}}@media (min-width:768px) and (max-width:991px){.right[data-v-7c3c79e6]{right:60px}}@media (min-width:992px) and (max-width:1199px){.right[data-v-7c3c79e6]{right:90px}}@media (min-width:1200px){.right[data-v-7c3c79e6]{right:100px}}@media (max-width:575px){.left[data-v-7c3c79e6]{left:16px}}@media (min-width:576px) and (max-width:767px){.left[data-v-7c3c79e6]{left:40px}}@media (min-width:768px) and (max-width:991px){.left[data-v-7c3c79e6]{left:60px}}@media (min-width:992px) and (max-width:1199px){.left[data-v-7c3c79e6]{left:90px}}@media (min-width:1200px){.left[data-v-7c3c79e6]{left:100px}}.left .about-us[data-v-7c3c79e6]{min-width:320px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/featured-content.vue?vue&type=template&id=7c3c79e6&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:("main-content d-flex " + _vm.mainPosition)},[_vm._ssrNode("<div class=\"img-container w-100\" data-v-7c3c79e6><img"+(_vm._ssrAttr("src",_vm.src))+" alt=\"Responsive image\""+(_vm._ssrClass(null,("responsive-image " + _vm.imgClass)))+" data-v-7c3c79e6></div> <div"+(_vm._ssrClass(null,("block " + _vm.backgroundColor + " " + _vm.boxPosition + " " + _vm.imgClass)))+" data-v-7c3c79e6><div"+(_vm._ssrClass(null,("block-content " + _vm.fontColor)))+" data-v-7c3c79e6><h2"+(_vm._ssrClass(null,_vm.titleClass ? _vm.titleClass : 'title'))+" data-v-7c3c79e6>"+_vm._ssrEscape("          \n        "+_vm._s(_vm.$t(_vm.title))+"\n      ")+"</h2> <p"+(_vm._ssrClass(null,_vm.textClass ? _vm.textClass : ''))+" data-v-7c3c79e6>"+_vm._ssrEscape(_vm._s(_vm.$t(_vm.text)))+"</p> <a"+(_vm._ssrAttr("href",_vm.link))+(_vm._ssrClass(null,_vm.fontColor))+" data-v-7c3c79e6><i"+(_vm._ssrClass(null,_vm.icon))+" data-v-7c3c79e6></i></a> <img"+(_vm._ssrAttr("src",__webpack_require__(20)))+" width=\"43\""+(_vm._ssrClass("mt-1",_vm.logoHome))+" data-v-7c3c79e6></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/featured-content.vue?vue&type=template&id=7c3c79e6&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/featured-content.vue?vue&type=script&lang=ts&
/* harmony default export */ var featured_contentvue_type_script_lang_ts_ = ({
  props: {
    mainPosition: String,
    title: String,
    titleClass: String,
    icon: String,
    text: String,
    textClass: String,
    src: String,
    imgClass: String,
    backgroundColor: String,
    fontColor: String,
    boxPosition: String,
    logoHome: String,
    link: String
  }
});
// CONCATENATED MODULE: ./components/featured-content.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_featured_contentvue_type_script_lang_ts_ = (featured_contentvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/featured-content.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(55)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_featured_contentvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "7c3c79e6",
  "fb337f6e"
  
)

/* harmony default export */ var featured_content = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 58:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(66);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("fcca3c3c", content, true, context)
};

/***/ }),

/***/ 59:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(68);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("417d296e", content, true, context)
};

/***/ }),

/***/ 64:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(86);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("3c986210", content, true, context)
};

/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_staff_vue_vue_type_style_index_0_id_2b07c06c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(58);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_staff_vue_vue_type_style_index_0_id_2b07c06c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_staff_vue_vue_type_style_index_0_id_2b07c06c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_staff_vue_vue_type_style_index_0_id_2b07c06c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_staff_vue_vue_type_style_index_0_id_2b07c06c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 66:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".container__card[data-v-2b07c06c]{min-height:250px}@media (max-width:575px){.container__card[data-v-2b07c06c]{min-height:200px!important}}.avatar[data-v-2b07c06c]{max-width:100%}.staff__name[data-v-2b07c06c]{font-family:\"Silka SemiBold\"!important;font-size:20px;line-height:20px}@media (max-width:575px){.staff__name[data-v-2b07c06c]{font-size:10px}}.staff__description[data-v-2b07c06c]{font-family:\"Silka Light\"!important;font-size:14px;line-height:15px}@media (max-width:575px){.staff__description[data-v-2b07c06c]{font-size:9px}}.staff__description a[data-v-2b07c06c],.staff__description a[data-v-2b07c06c] :focus,.staff__description a[data-v-2b07c06c]:hover,.staff__description a[data-v-2b07c06c] :visited{color:#000}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 67:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_subscription_vue_vue_type_style_index_0_id_30a452e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(59);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_subscription_vue_vue_type_style_index_0_id_30a452e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_subscription_vue_vue_type_style_index_0_id_30a452e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_subscription_vue_vue_type_style_index_0_id_30a452e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_subscription_vue_vue_type_style_index_0_id_30a452e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 68:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".container__form[data-v-30a452e2]{width:1200px;max-width:100%;margin:auto}.container__form .title[data-v-30a452e2]{font-family:\"Silka SemiBold\"!important;font-size:2.3rem}@media (max-width:575px){.container__form .title[data-v-30a452e2]{font-size:20px;line-height:20px}}.container__form .description[data-v-30a452e2]{font-family:\"Silka\";border-color:#222!important;font-size:18px;line-height:20px;font-weight:300;max-width:100%}@media (min-width:900px){.container__form .description[data-v-30a452e2]{width:440px}}@media (max-width:575px){.container__form .description[data-v-30a452e2]{font-size:10px}}.container__form .container__input[data-v-30a452e2]{width:calc(100% - 106px)}@media (max-width:575px){.container__form .form-control[data-v-30a452e2]{font-size:10px}}@media (max-width:575px){.container__form .btn-submit[data-v-30a452e2]{font-size:8px}}.container__form .input-group-text[data-v-30a452e2]{padding-left:0}.container__form .form-control[data-v-30a452e2]{font-family:\"Silka\"}.container__form button[data-v-30a452e2]{margin-top:5px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 80:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/card-staff.vue?vue&type=template&id=2b07c06c&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container__card"},[_vm._ssrNode("<img"+(_vm._ssrAttr("src",_vm.avatar))+" border=\"0\""+(_vm._ssrAttr("alt",("staff " + _vm.name)))+" class=\"avatar\" data-v-2b07c06c> <label class=\"staff__name mt-3\" data-v-2b07c06c>"+_vm._ssrEscape(_vm._s(_vm.name))+"</label> <div class=\"staff__description\" data-v-2b07c06c>"+_vm._ssrEscape("\n    "+_vm._s(_vm.$t(_vm.position))+"\n    ")+"<div class=\"mt-2\" data-v-2b07c06c></div> <i class=\"fas fa-arrow-right mr-2\" data-v-2b07c06c></i> <a"+(_vm._ssrAttr("href",_vm.linkedin))+" target=\"_blank\" data-v-2b07c06c>LinkedIn</a></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/card-staff.vue?vue&type=template&id=2b07c06c&scoped=true&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/card-staff.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};


let card_staffvue_type_script_lang_ts_default_1 = class default_1 extends external_nuxt_property_decorator_["Vue"] {};

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_staffvue_type_script_lang_ts_default_1.prototype, "avatar", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_staffvue_type_script_lang_ts_default_1.prototype, "name", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_staffvue_type_script_lang_ts_default_1.prototype, "position", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_staffvue_type_script_lang_ts_default_1.prototype, "linkedin", void 0);

card_staffvue_type_script_lang_ts_default_1 = __decorate([external_nuxt_property_decorator_["Component"]], card_staffvue_type_script_lang_ts_default_1);
/* harmony default export */ var card_staffvue_type_script_lang_ts_ = (card_staffvue_type_script_lang_ts_default_1);
// CONCATENATED MODULE: ./components/card-staff.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_card_staffvue_type_script_lang_ts_ = (card_staffvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/card-staff.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(65)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_card_staffvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "2b07c06c",
  "0631cddc"
  
)

/* harmony default export */ var card_staff = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 81:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/form-subscription.vue?vue&type=template&id=30a452e2&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container__form pt-md-5"},[_vm._ssrNode("<div class=\"row px-2 px-lg-4\" data-v-30a452e2><div class=\"col-12 col-lg-5\" data-v-30a452e2><div class=\"title text-left text-md-right pr-5 mb-4\" data-v-30a452e2>"+_vm._ssrEscape("\n        "+_vm._s(_vm.$t('components.forms.subscribe.title'))+"\n      ")+"</div></div> <div class=\"col-12 col-md-7 col-lg-6\" data-v-30a452e2><div class=\"description border-left pl-5\" data-v-30a452e2>"+_vm._ssrEscape("\n        "+_vm._s(_vm.$t('components.forms.subscribe.description'))+"\n      ")+"</div> <form id=\"subscription-form\" name=\"subscription-form\" data-v-30a452e2><div class=\"form-group pl-0 pl-md-5 mt-5\" data-v-30a452e2><div class=\"container__input d-inline-block\" data-v-30a452e2><div class=\"input-group mb-2\" data-v-30a452e2><div class=\"input-group-prepend\" data-v-30a452e2><div class=\"input-group-text\" data-v-30a452e2><i class=\"fas fa-arrow-right\" data-v-30a452e2></i></div></div> <input type=\"email\" name=\"email\" id=\"email\""+(_vm._ssrAttr("placeholder",_vm.$t('components.forms.subscribe.email')))+(_vm._ssrAttr("value",(_vm.formModel.email)))+" class=\"form-control\" data-v-30a452e2></div></div> <button type=\"submit\""+(_vm._ssrAttr("disabled",_vm.disabled))+" class=\"btn btn-submit uppercase float-right float-md-none mr-1 text-white d-inline-block\" data-v-30a452e2><i class=\"far fa-paper-plane text-with mr-2\" data-v-30a452e2></i>"+_vm._ssrEscape("\n            "+_vm._s(_vm.$t('components.forms.subscribe.submit'))+"\n          ")+"</button></div></form></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/form-subscription.vue?vue&type=template&id=30a452e2&scoped=true&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// EXTERNAL MODULE: external "emailjs-com"
var external_emailjs_com_ = __webpack_require__(52);
var external_emailjs_com_default = /*#__PURE__*/__webpack_require__.n(external_emailjs_com_);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/form-subscription.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let default_1 = class extends external_nuxt_property_decorator_["Vue"] {
  constructor() {
    super(...arguments);
    this.disabled = false;
    this.formModel = {
      email: '',
      contactType: 'Subscription'
    };
  }

  onSubmit() {
    if (!this.formModel.email) {
      const message = this.$i18n.t('components.forms.subscribe.error-email');
      this.$bvToast.toast(message, {
        title: `Error`,
        variant: 'danger',
        solid: true
      });
    } else {
      this.disabled = true;
      external_emailjs_com_default.a.sendForm(this.$config.emailjsService, this.$config.emailjsContact, 'subscription-form', this.$config.emailjsUserId).then(() => {
        const message = this.$i18n.t('components.forms.subscribe.sended');
        this.$bvToast.toast(message, {
          title: 'gobetween',
          variant: 'success',
          solid: true
        });
        this.formModel = {
          email: '',
          contactType: ''
        };
        this.disabled = false;
      }, error => {
        this.$bvToast.toast(error, {
          title: `Error`,
          variant: 'danger',
          solid: true
        });
        this.disabled = false;
      });
    }
  }

};
default_1 = __decorate([external_nuxt_property_decorator_["Component"]], default_1);
/* harmony default export */ var form_subscriptionvue_type_script_lang_ts_ = (default_1);
// CONCATENATED MODULE: ./components/form-subscription.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_form_subscriptionvue_type_script_lang_ts_ = (form_subscriptionvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/form-subscription.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(67)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_form_subscriptionvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "30a452e2",
  "135e4d27"
  
)

/* harmony default export */ var form_subscription = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_our_staff_vue_vue_type_style_index_0_id_3ce83771_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(64);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_our_staff_vue_vue_type_style_index_0_id_3ce83771_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_our_staff_vue_vue_type_style_index_0_id_3ce83771_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_our_staff_vue_vue_type_style_index_0_id_3ce83771_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_our_staff_vue_vue_type_style_index_0_id_3ce83771_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 86:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".container__staff[data-v-3ce83771]{max-width:100%;margin:40px auto}@media (max-width:575px){.container__staff[data-v-3ce83771]{margin:40px auto}.container__staff .page-title[data-v-3ce83771]{font-size:20px;margin-bottom:2em!important}}@media (min-width:576px) and (max-width:767px){.container__staff[data-v-3ce83771]{margin:50px 30px}}@media (min-width:768px) and (max-width:991px){.container__staff[data-v-3ce83771]{margin:50px 45px}}@media (min-width:992px) and (max-width:1199px){.container__staff[data-v-3ce83771]{margin:50px 80px}}@media (min-width:1200px){.container__staff[data-v-3ce83771]{margin:50px 100px}}.row[data-v-3ce83771]{margin-right:0;margin-left:0;padding:0!important}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 97:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/about-us-first-section.vue?vue&type=template&id=3d2b4ccf&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"about-us-first"},[_c('b-row',{staticClass:"w-100"},[_c('b-col',{staticClass:"pb-4 pb-md-0",attrs:{"md":"6"}},[_c('div',{staticClass:"top-section"},[_c('h3',[_vm._v("\n          "+_vm._s(_vm.$t('pages.about-us.first-section.title-a'))+"\n        ")])]),_vm._v(" "),_c('div',{staticClass:"text-block"},[_c('p',[_vm._v("\n          "+_vm._s(_vm.$t('pages.about-us.first-p'))+"\n        ")]),_vm._v(" "),_c('p',[_vm._v("\n          "+_vm._s(_vm.$t('pages.about-us.second-p'))+"\n        ")])])]),_vm._v(" "),_c('b-col',{attrs:{"md":"6"}},[_c('div',{staticClass:"top-section"},[_c('div',{staticClass:"image"},[_c('div',{staticClass:"logo"},[_c('img',{attrs:{"src":__webpack_require__(20)}})])])]),_vm._v(" "),_c('div',{staticClass:"text-block"},[_c('p',{staticClass:"speech pr-5"},[_vm._v("\n          "+_vm._s(_vm.$t('pages.about-us.testimony'))+"\n        ")]),_vm._v(" "),_c('p',{staticClass:"signature"},[_vm._v("Melina Delpontigo "),_c('span',{staticClass:"d-block"},[_vm._v("| "+_vm._s(_vm.$t('pages.about-us.first-section.signature-title')))])])])])],1)],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/about-us-first-section.vue?vue&type=template&id=3d2b4ccf&

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/about-us-first-section.vue

var script = {}


/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  script,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "ba107870"
  
)

/* harmony default export */ var about_us_first_section = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=about-us.js.map