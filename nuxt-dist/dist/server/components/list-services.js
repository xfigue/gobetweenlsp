exports.ids = [11,4];
exports.modules = {

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/list-services.vue?vue&type=template&id=257b9638&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',_vm._l((_vm.catalogServices),function(service){return _c('card-service',{key:service.name,attrs:{"name":service.name,"subtitle":service.subtitle,"description":service.description,"icon":service.icon,"rotate-icon":service.rotateIcon,"services":service.services}})}),1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/list-services.vue?vue&type=template&id=257b9638&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// EXTERNAL MODULE: ./helpers/catalogs.ts
var catalogs = __webpack_require__(63);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/list-services.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let default_1 = class extends external_nuxt_property_decorator_["Vue"] {
  constructor() {
    super(...arguments);
    this.catalogServices = catalogs["b" /* CatalogServices */];
  }

};
default_1 = __decorate([external_nuxt_property_decorator_["Component"]], default_1);
/* harmony default export */ var list_servicesvue_type_script_lang_ts_ = (default_1);
// CONCATENATED MODULE: ./components/list-services.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_list_servicesvue_type_script_lang_ts_ = (list_servicesvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/list-services.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_list_servicesvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "85919964"
  
)

/* harmony default export */ var list_services = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {CardService: __webpack_require__(84).default})


/***/ }),

/***/ 62:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(78);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("13fed5b6", content, true, context)
};

/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CatalogServices; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CatalogDetailServices; });
const CatalogServices = [{
  name: 'catalogs.services.field-locale.title',
  description: 'catalogs.services.field-locale.description',
  subtitle: 'catalogs.services.field-locale.subtitle',
  icon: 'fas fa-crosshairs',
  rotateIcon: false,
  services: ['catalogs.services.field-locale.service-technology', 'catalogs.services.field-locale.service-telecommunications', 'catalogs.services.field-locale.service-software', 'catalogs.services.field-locale.service-life-sciences', 'catalogs.services.field-locale.service-gaming', 'catalogs.services.field-locale.service-legal', 'catalogs.services.field-locale.service-education', 'catalogs.services.field-locale.service-travel', 'catalogs.services.field-locale.service-automotive', 'catalogs.services.field-locale.service-more']
}, {
  name: 'catalogs.services.cat-tools.title',
  description: 'catalogs.services.cat-tools.description',
  icon: 'fas fa-wrench',
  rotateIcon: true,
  services: ['catalogs.services.cat-tools.service-linguistic', 'catalogs.services.cat-tools.service-catalyst', 'catalogs.services.cat-tools.service-across', 'catalogs.services.cat-tools.service-trados-studio', 'catalogs.services.cat-tools.service-translation', 'catalogs.services.cat-tools.service-smartling', 'catalogs.services.cat-tools.service-passolo', 'catalogs.services.cat-tools.service-wordfast', 'catalogs.services.cat-tools.service-memoQ', 'catalogs.services.cat-tools.service-xtm', 'catalogs.services.cat-tools.service-xbench', 'catalogs.services.cat-tools.service-dtptools', 'catalogs.services.cat-tools.service-memsource', 'catalogs.services.cat-tools.service-lingotek', 'catalogs.services.cat-tools.service-verifika', 'catalogs.services.cat-tools.description']
}];
const CatalogDetailServices = [{
  icon: 'retweet.svg',
  showIcon: false,
  name: 'catalogs.services.detailServices.traslationAndLocalization.title',
  description: 'catalogs.services.detailServices.traslationAndLocalization.description'
}, {
  icon: 'check-circle.svg',
  showIcon: false,
  name: 'catalogs.services.detailServices.edition.title',
  description: 'catalogs.services.detailServices.edition.description'
}, {
  icon: 'eye.svg',
  name: 'catalogs.services.detailServices.audiovisual.title',
  showIcon: false,
  description: 'catalogs.services.detailServices.audiovisual.description'
}, {
  icon: 'ruler.svg',
  showIcon: false,
  name: 'catalogs.services.detailServices.desktopPublishing.title',
  description: 'catalogs.services.detailServices.desktopPublishing.description'
}, {
  icon: 'user-circle.svg',
  showIcon: false,
  name: 'catalogs.services.detailServices.customizedServices.title',
  description: 'catalogs.services.detailServices.customizedServices.description'
}, {
  icon: 'training.svg',
  showIcon: false,
  name: 'catalogs.services.detailServices.training.title',
  description: 'catalogs.services.detailServices.training.description',
  secondDescription: 'catalogs.services.detailServices.training.description-2'
}];

/***/ }),

/***/ 77:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_service_vue_vue_type_style_index_0_id_388f1e00_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(62);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_service_vue_vue_type_style_index_0_id_388f1e00_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_service_vue_vue_type_style_index_0_id_388f1e00_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_service_vue_vue_type_style_index_0_id_388f1e00_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_service_vue_vue_type_style_index_0_id_388f1e00_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 78:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".container__service[data-v-388f1e00]{margin-top:40px}@media (max-width:576px){.container__service[data-v-388f1e00]{padding:0 30px}}@media (min-width:576px) and (max-width:767px){.container__service[data-v-388f1e00]{padding:0 40px}}@media (min-width:768px) and (max-width:991px){.container__service[data-v-388f1e00]{padding:0 60px}}@media (min-width:992px) and (max-width:1199px){.container__service[data-v-388f1e00]{padding:0 90px}}@media (min-width:1200px){.container__service[data-v-388f1e00]{margin-top:96px;padding:0 100px}}.container__service .title[data-v-388f1e00]{font-family:\"Silka SemiBold\"!important;font-size:25px;line-height:25px}@media (max-width:576px){.container__service .title[data-v-388f1e00]{font-size:20px;line-height:20px}}.container__service .sub-title[data-v-388f1e00]{font-family:\"Silka Light\"!important;font-size:15px;line-height:25px;vertical-align:top}@media (max-width:576px){.container__service .sub-title[data-v-388f1e00]{font-size:10px;line-height:10px;margin-top:13px}}.container__service i[data-v-388f1e00]{color:#ea0029;font-size:15px;vertical-align:top;padding-top:4px;line-height:20px}.container__service i.rotate-icon[data-v-388f1e00]{transform:rotate(-90deg)}.container__service .service__detail[data-v-388f1e00]{font-family:\"Silka\";font-size:16px;line-height:16px}@media (max-width:576px){.container__service .service__detail[data-v-388f1e00]{font-size:10px;line-height:10px}}.container__service .service__detail[data-v-388f1e00]:before{content:\"\\a\";margin-left:10px;margin-right:10px;width:3px;height:3px;vertical-align:middle;border-radius:50%;background:#222;display:inline-block}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 84:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/card-service.vue?vue&type=template&id=388f1e00&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container__service pt-5"},[_vm._ssrNode("<div class=\"title d-flex align-items-center mr-5\" data-v-388f1e00><i"+(_vm._ssrClass(null,(_vm.icon + " " + (_vm.rotateIcon ? 'rotate-icon' : '') + " mr-2")))+" data-v-388f1e00></i><label class=\"mb-0\" data-v-388f1e00>"+_vm._ssrEscape(_vm._s(_vm.$t(_vm.name)))+"</label></div> <div class=\"ml-4 sub-title d-inline-block\" data-v-388f1e00>"+_vm._ssrEscape("\n    "+_vm._s(_vm.$t(_vm.description))+"\n    ")+((_vm.subtitle)?("<br data-v-388f1e00>"):"<!---->")+_vm._ssrEscape("\n    "+_vm._s(_vm.subtitle ? _vm.$t(_vm.subtitle) : '')+"\n  ")+"</div> <div class=\"row mx-0 px-0 mt-4\" data-v-388f1e00>"+(_vm._ssrList((_vm.services.filter(function (x) { return _vm.$t(x).length !== 0; })),function(service){return ("<div class=\"col-12 col-sm-6 pl-md-4 col-md-3 py-1 service__detail px-0\" data-v-388f1e00>"+_vm._ssrEscape("\n      "+_vm._s(_vm.$t(service))+"\n    ")+"</div>")}))+"</div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/card-service.vue?vue&type=template&id=388f1e00&scoped=true&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/card-service.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};


let card_servicevue_type_script_lang_ts_default_1 = class default_1 extends external_nuxt_property_decorator_["Vue"] {};

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_servicevue_type_script_lang_ts_default_1.prototype, "name", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_servicevue_type_script_lang_ts_default_1.prototype, "subtitle", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_servicevue_type_script_lang_ts_default_1.prototype, "description", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_servicevue_type_script_lang_ts_default_1.prototype, "icon", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: false
})], card_servicevue_type_script_lang_ts_default_1.prototype, "rotateIcon", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: () => []
})], card_servicevue_type_script_lang_ts_default_1.prototype, "services", void 0);

card_servicevue_type_script_lang_ts_default_1 = __decorate([external_nuxt_property_decorator_["Component"]], card_servicevue_type_script_lang_ts_default_1);
/* harmony default export */ var card_servicevue_type_script_lang_ts_ = (card_servicevue_type_script_lang_ts_default_1);
// CONCATENATED MODULE: ./components/card-service.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_card_servicevue_type_script_lang_ts_ = (card_servicevue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/card-service.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(77)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_card_servicevue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "388f1e00",
  "5de9abe7"
  
)

/* harmony default export */ var card_service = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=list-services.js.map