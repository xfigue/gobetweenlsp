exports.ids = [1];
exports.modules = {

/***/ 97:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/about-us-first-section.vue?vue&type=template&id=3d2b4ccf&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"about-us-first"},[_c('b-row',{staticClass:"w-100"},[_c('b-col',{staticClass:"pb-4 pb-md-0",attrs:{"md":"6"}},[_c('div',{staticClass:"top-section"},[_c('h3',[_vm._v("\n          "+_vm._s(_vm.$t('pages.about-us.first-section.title-a'))+"\n        ")])]),_vm._v(" "),_c('div',{staticClass:"text-block"},[_c('p',[_vm._v("\n          "+_vm._s(_vm.$t('pages.about-us.first-p'))+"\n        ")]),_vm._v(" "),_c('p',[_vm._v("\n          "+_vm._s(_vm.$t('pages.about-us.second-p'))+"\n        ")])])]),_vm._v(" "),_c('b-col',{attrs:{"md":"6"}},[_c('div',{staticClass:"top-section"},[_c('div',{staticClass:"image"},[_c('div',{staticClass:"logo"},[_c('img',{attrs:{"src":__webpack_require__(20)}})])])]),_vm._v(" "),_c('div',{staticClass:"text-block"},[_c('p',{staticClass:"speech pr-5"},[_vm._v("\n          "+_vm._s(_vm.$t('pages.about-us.testimony'))+"\n        ")]),_vm._v(" "),_c('p',{staticClass:"signature"},[_vm._v("Melina Delpontigo "),_c('span',{staticClass:"d-block"},[_vm._v("| "+_vm._s(_vm.$t('pages.about-us.first-section.signature-title')))])])])])],1)],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/about-us-first-section.vue?vue&type=template&id=3d2b4ccf&

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/about-us-first-section.vue

var script = {}


/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  script,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "ba107870"
  
)

/* harmony default export */ var about_us_first_section = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=about-us-first-section.js.map