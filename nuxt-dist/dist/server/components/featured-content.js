exports.ids = [7];
exports.modules = {

/***/ 54:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(56);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("ec8a999c", content, true, context)
};

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(54);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_featured_content_vue_vue_type_style_index_0_id_7c3c79e6_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 56:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".main-content[data-v-7c3c79e6]{position:relative}.main-content .img-container[data-v-7c3c79e6]{overflow-x:hidden}.main-content .img-container .responsive-image[data-v-7c3c79e6]{width:100%}@media (max-width:575px){.main-content .img-container .responsive-image[data-v-7c3c79e6]{width:180%}.main-content .img-container .responsive-image.home[data-v-7c3c79e6]{width:140%;margin-left:-23%}.main-content .img-container .responsive-image.services[data-v-7c3c79e6]{margin-left:-60%}.main-content .img-container .responsive-image.about-us[data-v-7c3c79e6]{margin-left:-20%;width:130%}.main-content .img-container .responsive-image.contact-us[data-v-7c3c79e6]{margin-left:-66%}.main-content .img-container .responsive-image.x-files[data-v-7c3c79e6]{margin-left:-57%}}.padding-right[data-v-7c3c79e6]{padding-right:100px}@media (max-width:575px){.padding-right[data-v-7c3c79e6]{padding-right:10px}}@media (min-width:576px) and (max-width:767px){.padding-right[data-v-7c3c79e6]{padding-right:40px}}@media (min-width:768px) and (max-width:991px){.padding-right[data-v-7c3c79e6]{padding-right:60px}}@media (min-width:992px) and (max-width:1199px){.padding-right[data-v-7c3c79e6]{padding-right:90px}}.padding-left[data-v-7c3c79e6]{padding-left:100px}@media (max-width:575px){.padding-left[data-v-7c3c79e6]{padding-left:10px}}@media (min-width:576px) and (max-width:767px){.padding-left[data-v-7c3c79e6]{padding-left:40px}}@media (min-width:768px) and (max-width:991px){.padding-left[data-v-7c3c79e6]{padding-left:60px}}@media (min-width:992px) and (max-width:1199px){.padding-left[data-v-7c3c79e6]{padding-left:90px}}.block[data-v-7c3c79e6]{position:absolute}@media (max-width:575px){.block[data-v-7c3c79e6]{width:184px;bottom:-40px}.block.about-us[data-v-7c3c79e6]{width:auto}.block.about-us .block-content[data-v-7c3c79e6]{padding:20px 35px}}@media (min-width:576px) and (max-width:767px){.block[data-v-7c3c79e6]{width:205px;bottom:-40px}.block.home[data-v-7c3c79e6]{margin-left:40px}.block.about-us[data-v-7c3c79e6]{width:auto}}@media (min-width:768px) and (max-width:991px){.block[data-v-7c3c79e6]{width:225px;bottom:-60px}.block.home[data-v-7c3c79e6]{margin-left:60px}.block.about-us[data-v-7c3c79e6]{width:auto}}@media (min-width:992px) and (max-width:1199px){.block[data-v-7c3c79e6]{width:255px;bottom:-60px}.block.home[data-v-7c3c79e6]{margin-left:90px}.block.about-us[data-v-7c3c79e6]{width:auto}}@media (min-width:1200px){.block[data-v-7c3c79e6]{width:429px;bottom:-60px}}.block-content[data-v-7c3c79e6]{overflow:hidden;padding:100px 60px}@media (max-width:575px){.block-content[data-v-7c3c79e6]{padding:32px}}@media (min-width:576px) and (max-width:767px){.block-content[data-v-7c3c79e6]{padding:42px}}@media (min-width:768px) and (max-width:991px){.block-content[data-v-7c3c79e6]{padding:50px}}@media (min-width:992px) and (max-width:1199px){.block-content[data-v-7c3c79e6]{padding:50px}}.block-content .about-us-text[data-v-7c3c79e6]{margin-top:0!important}.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-family:\"Silka Bold\";font-size:2rem}@media (max-width:575px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:18px}}@media (min-width:576px) and (max-width:767px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:20px;line-height:25px}}@media (min-width:768px) and (max-width:991px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:25px;line-height:25px}}@media (min-width:992px) and (max-width:1199px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:25px;line-height:25px}}@media (min-width:1200px){.block-content .about-us-text[data-v-7c3c79e6],.block-content .title[data-v-7c3c79e6]{font-size:40px;line-height:40px}}.block-content p[data-v-7c3c79e6]{font-family:\"Silka Light\";line-height:20px;margin-top:25px;margin-bottom:25px;font-size:1rem}@media (max-width:575px){.block-content p[data-v-7c3c79e6]{line-height:12px;font-size:9px;margin-top:9px;margin-bottom:10px}}@media (min-width:576px) and (max-width:767px){.block-content p[data-v-7c3c79e6]{font-size:12px;line-height:14px;margin-top:15px}}@media (min-width:768px) and (max-width:991px){.block-content p[data-v-7c3c79e6]{font-size:15px;line-height:16px;margin-top:20px}}@media (min-width:992px) and (max-width:1199px){.block-content p[data-v-7c3c79e6]{font-size:16px;line-height:18px}}@media (min-width:1200px){.block-content p[data-v-7c3c79e6]{font-size:16px;line-height:18px}}.block-content img[data-v-7c3c79e6]{margin-top:10px}.block-content img.display-none[data-v-7c3c79e6]{display:none!important}@media (max-width:575px){.block-content img[data-v-7c3c79e6]{width:15px;margin-top:0}}@media (min-width:576px) and (max-width:767px){.block-content img[data-v-7c3c79e6]{width:25px}}@media (min-width:768px) and (max-width:991px){.block-content img[data-v-7c3c79e6]{width:25px}}.block-background-gray[data-v-7c3c79e6]{background-color:#222}.block-background-white[data-v-7c3c79e6]{background-color:#fff}.block-background-home[data-v-7c3c79e6]{background-color:#fefcf3}.block-background-light[data-v-7c3c79e6]{background-color:#f4eceb}.font-color-white[data-v-7c3c79e6]{color:#fff}.font-color-blue[data-v-7c3c79e6]{color:#292c7e}.font-color-black[data-v-7c3c79e6]{color:#222}@media (max-width:575px){.right[data-v-7c3c79e6]{right:10px}}@media (min-width:576px) and (max-width:767px){.right[data-v-7c3c79e6]{right:40px}}@media (min-width:768px) and (max-width:991px){.right[data-v-7c3c79e6]{right:60px}}@media (min-width:992px) and (max-width:1199px){.right[data-v-7c3c79e6]{right:90px}}@media (min-width:1200px){.right[data-v-7c3c79e6]{right:100px}}@media (max-width:575px){.left[data-v-7c3c79e6]{left:16px}}@media (min-width:576px) and (max-width:767px){.left[data-v-7c3c79e6]{left:40px}}@media (min-width:768px) and (max-width:991px){.left[data-v-7c3c79e6]{left:60px}}@media (min-width:992px) and (max-width:1199px){.left[data-v-7c3c79e6]{left:90px}}@media (min-width:1200px){.left[data-v-7c3c79e6]{left:100px}}.left .about-us[data-v-7c3c79e6]{min-width:320px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/featured-content.vue?vue&type=template&id=7c3c79e6&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:("main-content d-flex " + _vm.mainPosition)},[_vm._ssrNode("<div class=\"img-container w-100\" data-v-7c3c79e6><img"+(_vm._ssrAttr("src",_vm.src))+" alt=\"Responsive image\""+(_vm._ssrClass(null,("responsive-image " + _vm.imgClass)))+" data-v-7c3c79e6></div> <div"+(_vm._ssrClass(null,("block " + _vm.backgroundColor + " " + _vm.boxPosition + " " + _vm.imgClass)))+" data-v-7c3c79e6><div"+(_vm._ssrClass(null,("block-content " + _vm.fontColor)))+" data-v-7c3c79e6><h2"+(_vm._ssrClass(null,_vm.titleClass ? _vm.titleClass : 'title'))+" data-v-7c3c79e6>"+_vm._ssrEscape("          \n        "+_vm._s(_vm.$t(_vm.title))+"\n      ")+"</h2> <p"+(_vm._ssrClass(null,_vm.textClass ? _vm.textClass : ''))+" data-v-7c3c79e6>"+_vm._ssrEscape(_vm._s(_vm.$t(_vm.text)))+"</p> <a"+(_vm._ssrAttr("href",_vm.link))+(_vm._ssrClass(null,_vm.fontColor))+" data-v-7c3c79e6><i"+(_vm._ssrClass(null,_vm.icon))+" data-v-7c3c79e6></i></a> <img"+(_vm._ssrAttr("src",__webpack_require__(20)))+" width=\"43\""+(_vm._ssrClass("mt-1",_vm.logoHome))+" data-v-7c3c79e6></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/featured-content.vue?vue&type=template&id=7c3c79e6&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/featured-content.vue?vue&type=script&lang=ts&
/* harmony default export */ var featured_contentvue_type_script_lang_ts_ = ({
  props: {
    mainPosition: String,
    title: String,
    titleClass: String,
    icon: String,
    text: String,
    textClass: String,
    src: String,
    imgClass: String,
    backgroundColor: String,
    fontColor: String,
    boxPosition: String,
    logoHome: String,
    link: String
  }
});
// CONCATENATED MODULE: ./components/featured-content.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_featured_contentvue_type_script_lang_ts_ = (featured_contentvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/featured-content.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(55)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_featured_contentvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "7c3c79e6",
  "fb337f6e"
  
)

/* harmony default export */ var featured_content = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=featured-content.js.map