exports.ids = [10];
exports.modules = {

/***/ 59:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(68);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("417d296e", content, true, context)
};

/***/ }),

/***/ 67:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_subscription_vue_vue_type_style_index_0_id_30a452e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(59);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_subscription_vue_vue_type_style_index_0_id_30a452e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_subscription_vue_vue_type_style_index_0_id_30a452e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_subscription_vue_vue_type_style_index_0_id_30a452e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_subscription_vue_vue_type_style_index_0_id_30a452e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 68:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".container__form[data-v-30a452e2]{width:1200px;max-width:100%;margin:auto}.container__form .title[data-v-30a452e2]{font-family:\"Silka SemiBold\"!important;font-size:2.3rem}@media (max-width:575px){.container__form .title[data-v-30a452e2]{font-size:20px;line-height:20px}}.container__form .description[data-v-30a452e2]{font-family:\"Silka\";border-color:#222!important;font-size:18px;line-height:20px;font-weight:300;max-width:100%}@media (min-width:900px){.container__form .description[data-v-30a452e2]{width:440px}}@media (max-width:575px){.container__form .description[data-v-30a452e2]{font-size:10px}}.container__form .container__input[data-v-30a452e2]{width:calc(100% - 106px)}@media (max-width:575px){.container__form .form-control[data-v-30a452e2]{font-size:10px}}@media (max-width:575px){.container__form .btn-submit[data-v-30a452e2]{font-size:8px}}.container__form .input-group-text[data-v-30a452e2]{padding-left:0}.container__form .form-control[data-v-30a452e2]{font-family:\"Silka\"}.container__form button[data-v-30a452e2]{margin-top:5px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 81:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/form-subscription.vue?vue&type=template&id=30a452e2&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container__form pt-md-5"},[_vm._ssrNode("<div class=\"row px-2 px-lg-4\" data-v-30a452e2><div class=\"col-12 col-lg-5\" data-v-30a452e2><div class=\"title text-left text-md-right pr-5 mb-4\" data-v-30a452e2>"+_vm._ssrEscape("\n        "+_vm._s(_vm.$t('components.forms.subscribe.title'))+"\n      ")+"</div></div> <div class=\"col-12 col-md-7 col-lg-6\" data-v-30a452e2><div class=\"description border-left pl-5\" data-v-30a452e2>"+_vm._ssrEscape("\n        "+_vm._s(_vm.$t('components.forms.subscribe.description'))+"\n      ")+"</div> <form id=\"subscription-form\" name=\"subscription-form\" data-v-30a452e2><div class=\"form-group pl-0 pl-md-5 mt-5\" data-v-30a452e2><div class=\"container__input d-inline-block\" data-v-30a452e2><div class=\"input-group mb-2\" data-v-30a452e2><div class=\"input-group-prepend\" data-v-30a452e2><div class=\"input-group-text\" data-v-30a452e2><i class=\"fas fa-arrow-right\" data-v-30a452e2></i></div></div> <input type=\"email\" name=\"email\" id=\"email\""+(_vm._ssrAttr("placeholder",_vm.$t('components.forms.subscribe.email')))+(_vm._ssrAttr("value",(_vm.formModel.email)))+" class=\"form-control\" data-v-30a452e2></div></div> <button type=\"submit\""+(_vm._ssrAttr("disabled",_vm.disabled))+" class=\"btn btn-submit uppercase float-right float-md-none mr-1 text-white d-inline-block\" data-v-30a452e2><i class=\"far fa-paper-plane text-with mr-2\" data-v-30a452e2></i>"+_vm._ssrEscape("\n            "+_vm._s(_vm.$t('components.forms.subscribe.submit'))+"\n          ")+"</button></div></form></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/form-subscription.vue?vue&type=template&id=30a452e2&scoped=true&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// EXTERNAL MODULE: external "emailjs-com"
var external_emailjs_com_ = __webpack_require__(52);
var external_emailjs_com_default = /*#__PURE__*/__webpack_require__.n(external_emailjs_com_);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/form-subscription.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let default_1 = class extends external_nuxt_property_decorator_["Vue"] {
  constructor() {
    super(...arguments);
    this.disabled = false;
    this.formModel = {
      email: '',
      contactType: 'Subscription'
    };
  }

  onSubmit() {
    if (!this.formModel.email) {
      const message = this.$i18n.t('components.forms.subscribe.error-email');
      this.$bvToast.toast(message, {
        title: `Error`,
        variant: 'danger',
        solid: true
      });
    } else {
      this.disabled = true;
      external_emailjs_com_default.a.sendForm(this.$config.emailjsService, this.$config.emailjsContact, 'subscription-form', this.$config.emailjsUserId).then(() => {
        const message = this.$i18n.t('components.forms.subscribe.sended');
        this.$bvToast.toast(message, {
          title: 'gobetween',
          variant: 'success',
          solid: true
        });
        this.formModel = {
          email: '',
          contactType: ''
        };
        this.disabled = false;
      }, error => {
        this.$bvToast.toast(error, {
          title: `Error`,
          variant: 'danger',
          solid: true
        });
        this.disabled = false;
      });
    }
  }

};
default_1 = __decorate([external_nuxt_property_decorator_["Component"]], default_1);
/* harmony default export */ var form_subscriptionvue_type_script_lang_ts_ = (default_1);
// CONCATENATED MODULE: ./components/form-subscription.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_form_subscriptionvue_type_script_lang_ts_ = (form_subscriptionvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/form-subscription.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(67)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_form_subscriptionvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "30a452e2",
  "135e4d27"
  
)

/* harmony default export */ var form_subscription = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=form-subscription.js.map