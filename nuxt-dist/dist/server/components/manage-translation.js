exports.ids = [12];
exports.modules = {

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/manage-translation.vue?vue&type=template&id=7dd59304&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container__manage__translations"},[_vm._ssrNode("<div class=\"row\" data-v-7dd59304><div class=\"col-md-12 col-lg-6 first-column\" data-v-7dd59304><div class=\"last-title title text-white\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.title')))+"</div> <p class=\"text-white hide-md\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.first-description')))+"</p> <p class=\"text-white hide-md\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.second-description')))+"</p> <i class=\"fas fa-arrow-down text-white d-block hide-md\" data-v-7dd59304></i> <a href=\"/files/gobetween_institucional.pdf\" download type=\"button\" class=\"btn bg-white mt-5 px-5 py-3 rounded-0 hide-md text-uppercase\" data-v-7dd59304><i class=\"fas fa-search mr-2\" data-v-7dd59304></i>"+_vm._ssrEscape("\n        "+_vm._s(_vm.$t('components.manage-translation.find-more-about-us'))+"\n      ")+"</a></div> <div class=\"col-md-12 col-lg-6 last-column\" data-v-7dd59304><div class=\"row mx-0 px-0\" data-v-7dd59304><div class=\"col-6\" data-v-7dd59304><div class=\"item-title\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.expertise-title')))+"</div> <div class=\"item-description pr-3 pr-md-0 mt-3\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.expertise-description')))+"</div></div> <div class=\"col-6\" data-v-7dd59304><div class=\"item-title\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.quality-title')))+"</div> <div class=\"item-description pr-3 pr-md-0 mt-3\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.quality-description')))+"</div></div> <div class=\"col-6 mt-5\" data-v-7dd59304><div class=\"item-title\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.availability-title')))+"</div> <div class=\"item-description pr-3 pr-md-0 mt-3\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.availability-description')))+"</div></div> <div class=\"col-6 mt-5\" data-v-7dd59304><div class=\"item-title\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.competitive-rates-title')))+"</div> <div class=\"item-description pr-3 pr-md-0 mt-3\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.competitive-rates-description')))+"</div></div></div></div> <div class=\"col-12 mt-5 show-md\" data-v-7dd59304><p class=\"text-white last-title\" data-v-7dd59304>"+_vm._ssrEscape(_vm._s(_vm.$t('components.manage-translation.second-description')))+"</p> <i class=\"fas fa-arrow-down text-white d-block\" data-v-7dd59304></i> <a href=\"/files/gobetween_institucional.pdf\" download class=\"btn bg-white mt-5 px-3 py-2 rounded-0 text-uppercase\" data-v-7dd59304><i class=\"fas fa-search mr-2\" data-v-7dd59304></i>"+_vm._ssrEscape("\n        "+_vm._s(_vm.$t('components.manage-translation.find-more-about-us'))+"\n      ")+"</a></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/manage-translation.vue?vue&type=template&id=7dd59304&scoped=true&

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/manage-translation.vue

var script = {}
function injectStyles (context) {
  
  var style0 = __webpack_require__(91)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  script,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "7dd59304",
  "72c85b78"
  
)

/* harmony default export */ var manage_translation = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 71:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(92);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("623f5c3a", content, true, context)
};

/***/ }),

/***/ 91:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_manage_translation_vue_vue_type_style_index_0_id_7dd59304_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(71);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_manage_translation_vue_vue_type_style_index_0_id_7dd59304_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_manage_translation_vue_vue_type_style_index_0_id_7dd59304_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_manage_translation_vue_vue_type_style_index_0_id_7dd59304_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_manage_translation_vue_vue_type_style_index_0_id_7dd59304_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 92:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".container__manage__translations[data-v-7dd59304]{padding:180px 0;background:#ea0029}@media (max-width:992px){.container__manage__translations>.row[data-v-7dd59304]{padding:0}}.container__manage__translations .col-md-12.first-column[data-v-7dd59304]{padding-left:80px}@media (max-width:992px){.container__manage__translations .col-md-12.first-column[data-v-7dd59304]{padding-left:0}}.container__manage__translations .col-md-12.last-column[data-v-7dd59304]{padding-right:80px}@media (max-width:992px){.container__manage__translations .col-md-12.last-column[data-v-7dd59304]{margin-top:50px;padding:0!important}}@media (max-width:992px){.container__manage__translations .col-md-12.last-column .col-6[data-v-7dd59304]{padding:0}}.container__manage__translations .col-md-12 .title[data-v-7dd59304]{font-size:45px;line-height:50px;width:500px;max-width:100%}@media (max-width:992px){.container__manage__translations .col-md-12 .title[data-v-7dd59304]{font-size:20px;line-height:20px;width:280px;max-width:100%}}.container__manage__translations .col-md-12 p[data-v-7dd59304]{font-family:\"Silka\";font-size:16px;line-height:20px;font-weight:300;width:450px;max-width:100%;margin:50px 0}.container__manage__translations .btn[data-v-7dd59304]{font-family:\"Silka\";font-size:15px}.container__manage__translations .btn[data-v-7dd59304]:hover{border:1px solid #000}@media (max-width:992px){.container__manage__translations .btn[data-v-7dd59304]{font-size:13px}}.container__manage__translations .item-title[data-v-7dd59304]{font-family:\"Silka Semibold\";display:flex;height:50px;align-items:start;font-size:20px;line-height:22px;font-weight:500;padding-left:25px;border-left:1px solid #fff;color:#fff;margin-bottom:25px}@media (max-width:992px){.container__manage__translations .item-title[data-v-7dd59304]{font-size:10px;line-height:10px;height:35px}}.container__manage__translations .item-description[data-v-7dd59304]{font-family:\"Silka\";font-size:18px;line-height:20px;font-weight:300;padding-left:26px;color:#fff}@media (max-width:992px){.container__manage__translations .item-description[data-v-7dd59304]{font-size:10px;line-height:10px}}@media (max-width:992px){.container__manage__translations[data-v-7dd59304]{padding:100px 25px 60px}}@media (max-width:575px){.container__manage__translations[data-v-7dd59304]{width:calc(100% - 17px);margin-left:17px}}@media (min-width:576px) and (max-width:767px){.container__manage__translations[data-v-7dd59304]{width:calc(100% - 41px);margin-left:41px}}@media (min-width:768px) and (max-width:991px){.container__manage__translations[data-v-7dd59304]{width:calc(100% - 61px);margin-left:61px}}@media (min-width:992px) and (max-width:1199px){.container__manage__translations[data-v-7dd59304]{width:calc(100% - 91px);margin-left:91px}}@media (min-width:1200px){.container__manage__translations[data-v-7dd59304]{width:calc(100% - 106px);margin-left:101px}}.container__manage__translations .last-title[data-v-7dd59304]{font-family:\"Silka Bold\"}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=manage-translation.js.map