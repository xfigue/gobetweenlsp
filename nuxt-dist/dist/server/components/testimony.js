exports.ids = [15];
exports.modules = {

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/testimony.vue?vue&type=template&id=1eace82d&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"testimony mt-5 mt-md-0"},[_vm._ssrNode("<h2 class=\"mb-md-5 mb-4 px-2 page-title\">"+_vm._ssrEscape(_vm._s(_vm.$t('components.testimony.title')))+"</h2> "),_c('b-row',{staticClass:"w-100"},[_c('b-col',{staticClass:"pb-5 pb-md-0",attrs:{"md":"12"}},[_c('b-carousel',{attrs:{"id":"carousel-1","interval":10000,"controls":"","indicators":""},on:{"sliding-start":_vm.onSlideStart,"sliding-end":_vm.onSlideEnd},model:{value:(_vm.slide),callback:function ($$v) {_vm.slide=$$v},expression:"slide"}},_vm._l((_vm.testimonyList),function(testimony,index){return _c('b-carousel-slide',{key:index},[_c('div',{staticClass:"text-block"},[_c('p',{staticClass:"text"},[_vm._v("\n              "+_vm._s(_vm.$t(testimony.text))+"\n            ")]),_vm._v(" "),_c('p',{staticClass:"signature mt-5"},[_vm._v(_vm._s(testimony.signature))])])])}),1)],1)],1)],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/testimony.vue?vue&type=template&id=1eace82d&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/testimony.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var testimonyvue_type_script_lang_js_ = ({
  data() {
    return {
      slide: 0,
      sliding: null,
      testimonyList: [{
        text: 'pages.home.testimony-1',
        signature: 'Alejandra Pizarro - Translation Line Manager'
      }, {
        text: 'pages.home.testimony-2',
        signature: 'Alejandra Andreu - Directora de producción'
      }, {
        text: 'pages.home.testimony-3',
        signature: 'Anabela Hotian - Localization Project Manager'
      }, {
        text: 'pages.home.testimony-4',
        signature: 'Virginia Reggiardo - Traductora'
      }, {
        text: 'pages.home.testimony-5',
        signature: 'Roberta Wersio - Traductora'
      }, {
        text: 'pages.home.testimony-6',
        signature: 'Yamila Razzetto - Translator'
      }]
    };
  },

  methods: {
    onSlideStart(slide) {
      this.sliding = true;
    },

    onSlideEnd(slide) {
      this.sliding = false;
    }

  }
});
// CONCATENATED MODULE: ./components/testimony.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_testimonyvue_type_script_lang_js_ = (testimonyvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/testimony.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_testimonyvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "47088d25"
  
)

/* harmony default export */ var testimony = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=testimony.js.map