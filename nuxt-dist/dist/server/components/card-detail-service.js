exports.ids = [2];
exports.modules = {

/***/ 61:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(76);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("3faf0b68", content, true, context)
};

/***/ }),

/***/ 75:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_detail_service_vue_vue_type_style_index_0_id_741a522a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(61);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_detail_service_vue_vue_type_style_index_0_id_741a522a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_detail_service_vue_vue_type_style_index_0_id_741a522a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_detail_service_vue_vue_type_style_index_0_id_741a522a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_detail_service_vue_vue_type_style_index_0_id_741a522a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 76:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".card-detail[data-v-741a522a]{background-color:#f4eceb;min-height:355px}@media (max-width:576px){.card-detail[data-v-741a522a]{padding:18px;min-height:0}}@media (min-width:576px) and (max-width:767px){.card-detail[data-v-741a522a]{padding:26px;min-height:0}}@media (min-width:768px) and (max-width:991px){.card-detail[data-v-741a522a]{padding:32px;min-height:0}}@media (min-width:992px){.card-detail[data-v-741a522a]{padding:55px}}.card-detail .title[data-v-741a522a]{font-family:\"Silka SemiBold\"!important;font-size:16px;line-height:22px;vertical-align:top}@media (max-width:576px){.card-detail .title[data-v-741a522a]{font-size:10px;line-height:10px}}.card-detail .title i[data-v-741a522a]{color:#ea0029;font-size:20px;line-height:20px}@media (max-width:576px){.card-detail .title i[data-v-741a522a]{font-size:10px;line-height:10px}}@media (min-width:576px) and (max-width:767px){.card-detail .title i[data-v-741a522a]{margin-right:15px!important}}.card-detail .description[data-v-741a522a]{font-family:\"Silka Light\"!important;font-size:14px;line-height:20px}@media (max-width:576px){.card-detail .description[data-v-741a522a]{font-size:10px;line-height:10px;margin-top:17px!important;margin-left:39px;margin-bottom:0}}@media (min-width:992px){.card-detail .description[data-v-741a522a]{height:100%;font-size:14px;margin-left:40px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/card-detail-service.vue?vue&type=template&id=741a522a&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('b-container',{staticClass:"card-detail"},[_c('div',[_c('div',{staticClass:"title"},[(_vm.showIcon)?_c('i',{class:(_vm.icon + " mr-md-3")}):_vm._e(),_vm._v(" "),(!_vm.showIcon)?_c('img',{attrs:{"width":"20","src":("/images/" + _vm.icon)}}):_vm._e(),_vm._v(" "),_c('label',{staticClass:"ml-3 mb-0"},[_vm._v("\n        "+_vm._s(_vm.$t(_vm.name))+" \n      ")])]),_vm._v(" "),_c('p',{staticClass:"description mt-4"},[_vm._v(" \n      "+_vm._s(_vm.$t(_vm.description))+"\n      "),(_vm.secondDescription)?_c('br'):_vm._e(),_vm._v("\n      "+_vm._s(_vm.secondDescription ? _vm.$t(_vm.secondDescription) : '')+"\n    ")])])])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/card-detail-service.vue?vue&type=template&id=741a522a&scoped=true&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/card-detail-service.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};


let card_detail_servicevue_type_script_lang_ts_default_1 = class default_1 extends external_nuxt_property_decorator_["Vue"] {};

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_detail_servicevue_type_script_lang_ts_default_1.prototype, "icon", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_detail_servicevue_type_script_lang_ts_default_1.prototype, "name", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_detail_servicevue_type_script_lang_ts_default_1.prototype, "description", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_detail_servicevue_type_script_lang_ts_default_1.prototype, "secondDescription", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: true
})], card_detail_servicevue_type_script_lang_ts_default_1.prototype, "showIcon", void 0);

card_detail_servicevue_type_script_lang_ts_default_1 = __decorate([external_nuxt_property_decorator_["Component"]], card_detail_servicevue_type_script_lang_ts_default_1);
/* harmony default export */ var card_detail_servicevue_type_script_lang_ts_ = (card_detail_servicevue_type_script_lang_ts_default_1);
// CONCATENATED MODULE: ./components/card-detail-service.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_card_detail_servicevue_type_script_lang_ts_ = (card_detail_servicevue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/card-detail-service.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(75)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_card_detail_servicevue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "741a522a",
  "0564a8f7"
  
)

/* harmony default export */ var card_detail_service = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=card-detail-service.js.map