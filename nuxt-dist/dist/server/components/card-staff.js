exports.ids = [5];
exports.modules = {

/***/ 58:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(66);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("fcca3c3c", content, true, context)
};

/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_staff_vue_vue_type_style_index_0_id_2b07c06c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(58);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_staff_vue_vue_type_style_index_0_id_2b07c06c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_staff_vue_vue_type_style_index_0_id_2b07c06c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_staff_vue_vue_type_style_index_0_id_2b07c06c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_card_staff_vue_vue_type_style_index_0_id_2b07c06c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 66:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".container__card[data-v-2b07c06c]{min-height:250px}@media (max-width:575px){.container__card[data-v-2b07c06c]{min-height:200px!important}}.avatar[data-v-2b07c06c]{max-width:100%}.staff__name[data-v-2b07c06c]{font-family:\"Silka SemiBold\"!important;font-size:20px;line-height:20px}@media (max-width:575px){.staff__name[data-v-2b07c06c]{font-size:10px}}.staff__description[data-v-2b07c06c]{font-family:\"Silka Light\"!important;font-size:14px;line-height:15px}@media (max-width:575px){.staff__description[data-v-2b07c06c]{font-size:9px}}.staff__description a[data-v-2b07c06c],.staff__description a[data-v-2b07c06c] :focus,.staff__description a[data-v-2b07c06c]:hover,.staff__description a[data-v-2b07c06c] :visited{color:#000}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 80:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/card-staff.vue?vue&type=template&id=2b07c06c&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container__card"},[_vm._ssrNode("<img"+(_vm._ssrAttr("src",_vm.avatar))+" border=\"0\""+(_vm._ssrAttr("alt",("staff " + _vm.name)))+" class=\"avatar\" data-v-2b07c06c> <label class=\"staff__name mt-3\" data-v-2b07c06c>"+_vm._ssrEscape(_vm._s(_vm.name))+"</label> <div class=\"staff__description\" data-v-2b07c06c>"+_vm._ssrEscape("\n    "+_vm._s(_vm.$t(_vm.position))+"\n    ")+"<div class=\"mt-2\" data-v-2b07c06c></div> <i class=\"fas fa-arrow-right mr-2\" data-v-2b07c06c></i> <a"+(_vm._ssrAttr("href",_vm.linkedin))+" target=\"_blank\" data-v-2b07c06c>LinkedIn</a></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/card-staff.vue?vue&type=template&id=2b07c06c&scoped=true&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/card-staff.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};


let card_staffvue_type_script_lang_ts_default_1 = class default_1 extends external_nuxt_property_decorator_["Vue"] {};

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_staffvue_type_script_lang_ts_default_1.prototype, "avatar", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_staffvue_type_script_lang_ts_default_1.prototype, "name", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_staffvue_type_script_lang_ts_default_1.prototype, "position", void 0);

__decorate([Object(external_nuxt_property_decorator_["Prop"])({
  default: ''
})], card_staffvue_type_script_lang_ts_default_1.prototype, "linkedin", void 0);

card_staffvue_type_script_lang_ts_default_1 = __decorate([external_nuxt_property_decorator_["Component"]], card_staffvue_type_script_lang_ts_default_1);
/* harmony default export */ var card_staffvue_type_script_lang_ts_ = (card_staffvue_type_script_lang_ts_default_1);
// CONCATENATED MODULE: ./components/card-staff.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_card_staffvue_type_script_lang_ts_ = (card_staffvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/card-staff.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(65)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_card_staffvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "2b07c06c",
  "0631cddc"
  
)

/* harmony default export */ var card_staff = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=card-staff.js.map