exports.ids = [9];
exports.modules = {

/***/ 69:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(88);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("6747b29c", content, true, context)
};

/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_contact_vue_vue_type_style_index_0_id_e7653e62_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(69);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_contact_vue_vue_type_style_index_0_id_e7653e62_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_contact_vue_vue_type_style_index_0_id_e7653e62_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_contact_vue_vue_type_style_index_0_id_e7653e62_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_usr_local_lib_node_modules_nuxt_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_usr_local_lib_node_modules_nuxt_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_form_contact_vue_vue_type_style_index_0_id_e7653e62_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 88:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".container__form[data-v-e7653e62]{padding:220px 0 180px;background-color:#fefcf3;margin-top:0}@media (max-width:576px){.container__form[data-v-e7653e62]{margin-left:0;padding:100px 25px}}@media (min-width:576px) and (max-width:767px){.container__form[data-v-e7653e62]{margin-left:0;padding:200px 25px;margin-left:40px!important}}@media (min-width:768px) and (max-width:991px){.container__form[data-v-e7653e62]{margin-left:0;padding:200px 25px;margin-left:60px!important}}@media (min-width:992px) and (max-width:1199px){.container__form[data-v-e7653e62]{margin-left:0;padding:200px 25px;margin-left:90px!important}}@media (min-width:1200px){.container__form[data-v-e7653e62]{margin-left:100px!important;margin-right:100px!important}}.container__form .title[data-v-e7653e62]{font-family:\"Silka Bold\"!important;line-height:40px;width:100%;max-width:100%}@media (max-width:576px){.container__form .title[data-v-e7653e62]{font-size:20px;line-height:30px}}@media (min-width:576px) and (max-width:767px){.container__form .title[data-v-e7653e62]{font-size:25px}}@media (min-width:768px) and (max-width:991px){.container__form .title[data-v-e7653e62]{font-size:30px}}@media (min-width:992px) and (max-width:1199px){.container__form .title[data-v-e7653e62]{font-size:35px}}@media (min-width:1200px){.container__form .title[data-v-e7653e62]{font-size:40px}}.container__form textarea[data-v-e7653e62]{min-height:150px}.container__form .form-control[data-v-e7653e62]{padding-left:0!important}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: /usr/local/lib/node_modules/nuxt/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/form-contact.vue?vue&type=template&id=e7653e62&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container__form"},[_vm._ssrNode("<div class=\"row px-1 px-md-0\" data-v-e7653e62><div class=\"col-12 col-lg-7\" data-v-e7653e62><div class=\"title text-left pl-md-5 pr-md-5 mb-4\" data-v-e7653e62>"+_vm._ssrEscape("\n        "+_vm._s(_vm.$t('components.forms.contact.title'))+"\n      ")+"</div></div> <div class=\"col-12 col-lg-5\" data-v-e7653e62><form id=\"contact-form\" name=\"contact-form\" data-v-e7653e62><div class=\"form-group\" data-v-e7653e62><input type=\"text\" name=\"name\" id=\"name\""+(_vm._ssrAttr("placeholder",_vm.$t('components.forms.contact.name')))+(_vm._ssrAttr("value",(_vm.formModel.name)))+" class=\"form-control\" data-v-e7653e62></div> <div class=\"form-group mt-2\" data-v-e7653e62><input type=\"email\" name=\"email\" id=\"email\""+(_vm._ssrAttr("placeholder",_vm.$t('components.forms.contact.email')))+(_vm._ssrAttr("value",(_vm.formModel.email)))+" class=\"form-control\" data-v-e7653e62></div> <div class=\"form-group mt-2\" data-v-e7653e62><textarea type=\"message\" name=\"message\" id=\"message\""+(_vm._ssrAttr("placeholder",_vm.$t('components.forms.contact.message')))+" class=\"form-control\" data-v-e7653e62>"+_vm._ssrEscape(_vm._s(_vm.formModel.message))+"</textarea> <input type=\"hidden\" name=\"contactType\" id=\"contactType\""+(_vm._ssrAttr("value",(_vm.formModel.contactType)))+" data-v-e7653e62></div> <button type=\"submit\""+(_vm._ssrAttr("disabled",_vm.disabled))+" class=\"btn btn-submit uppercase float-right float-md-none text-white\" data-v-e7653e62><i class=\"far fa-paper-plane text-with mr-2\" data-v-e7653e62></i>"+_vm._ssrEscape("\n          "+_vm._s(_vm.$t('components.forms.contact.submit'))+"\n        ")+"</button></form></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/form-contact.vue?vue&type=template&id=e7653e62&scoped=true&

// EXTERNAL MODULE: external "nuxt-property-decorator"
var external_nuxt_property_decorator_ = __webpack_require__(2);

// EXTERNAL MODULE: external "emailjs-com"
var external_emailjs_com_ = __webpack_require__(52);
var external_emailjs_com_default = /*#__PURE__*/__webpack_require__.n(external_emailjs_com_);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--12-0!./node_modules/ts-loader??ref--12-1!/usr/local/lib/node_modules/nuxt/node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/form-contact.vue?vue&type=script&lang=ts&
var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};



let default_1 = class extends external_nuxt_property_decorator_["Vue"] {
  constructor() {
    super(...arguments);
    this.disabled = false;
    this.formModel = {
      name: '',
      email: '',
      message: '',
      contactType: 'Contact'
    };
  }

  onSubmit() {
    if (!this.formModel.name || !this.formModel.email) {
      const message = this.$i18n.t('components.forms.contact.error-form');
      this.$bvToast.toast(message, {
        title: `Error`,
        variant: 'danger',
        solid: true
      });
    } else {
      this.disabled = true;
      external_emailjs_com_default.a.sendForm(this.$config.emailjsService, this.$config.emailjsContact, 'contact-form', this.$config.emailjsUserId).then(() => {
        const message = this.$i18n.t('components.forms.contact.sended');
        this.$bvToast.toast(message, {
          title: 'gobetween',
          variant: 'success',
          solid: true
        });
        this.formModel = { ...this.formModel,
          name: '',
          email: '',
          message: ''
        };
        this.disabled = false;
      }, error => {
        this.$bvToast.toast(error, {
          title: `Error`,
          variant: 'danger',
          solid: true
        });
        this.disabled = false;
      });
    }
  }

};
default_1 = __decorate([external_nuxt_property_decorator_["Component"]], default_1);
/* harmony default export */ var form_contactvue_type_script_lang_ts_ = (default_1);
// CONCATENATED MODULE: ./components/form-contact.vue?vue&type=script&lang=ts&
 /* harmony default export */ var components_form_contactvue_type_script_lang_ts_ = (form_contactvue_type_script_lang_ts_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/form-contact.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(87)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_form_contactvue_type_script_lang_ts_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "e7653e62",
  "76ca00f4"
  
)

/* harmony default export */ var form_contact = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=form-contact.js.map